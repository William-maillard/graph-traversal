/* Les membres du groupe de TP (groupes de 2 au maximum, redoublants seuls) :
MAILLARD William
*/
#include <stdio.h>
#include <stdlib.h>
#include "include/liste.h"
#include "include/xmalloc.h"
#include "include/graphe_TLA.h"


/****************** structure Tparcours  *************************/ 
/* cette structure sert à stocker les tableaux utilisés pendant le parcours:
  - couleur[] :  couleur[i] devra valoir soit BLANC, GRIS ou NOIR (pseudo-constantes définies ci-dessous)  
  - pere[]  : pour stocker l'arborescence de parcours. 
              par convention on devra avoir pere[racine] = 0
  - prof[]  : la profondeur des sommets dans l'arborescence.
              On devra avoir : prof[racine] = 0 et pour les autres sommets : prof[x] = prof[pere[x]] + 1 
ainsi que:
  - nb_sommets : le nombre de sommets du graphe
  - nb_sommets_explores : le nombre de sommets explorés par le parcours.
  Cette structure devra être initialisée avant de commencer le parcours.
*/
#define BLANC 1 /* valeurs possibles dans le tableau couleur */
#define GRIS 2
#define NOIR 3

typedef struct parcours {
  int nb_sommets; 
  int nb_sommets_explores; 
  int *couleur;
  int *pere;
  int *prof;
} Tparcours;

Tparcours *new_parcours(int n) {
  /* alloue la mémoire pour une structure Tparcours sur un graphe à n
     sommets et retourne un pointeur dessus. 
     Ne fait aucune initialisation à part l'initialisation de nb_sommets. */
  Tparcours *p;

  p = (Tparcours*) xmalloc(sizeof(Tparcours));
  p->couleur = (int *) xmalloc((n+1)*sizeof(int));
  p->pere = (int *) xmalloc((n+1)*sizeof(int));
  p->prof = (int *) xmalloc((n+1)*sizeof(int));
  p->nb_sommets = n;
  return p;
}

void afficher_parcours(Tparcours *p) {
  /* affiche le contenu des tableaux pere, couleur et prof du parcours
     ainsi que le nombre de sommets explorés */
  int i;

  printf("Contenu des tableaux :\n");
  printf("          ");
  for (i=1; i<= p->nb_sommets; i++) 
    printf("%3d",i);
  printf("\ncouleur : ");
  for (i=1; i<=  p->nb_sommets; i++) {
    if (p->couleur[i] == BLANC) printf("  B");
    if (p->couleur[i] == NOIR) printf("  N");
    if (p->couleur[i] == GRIS) printf("  G");
  } 
  printf("\npere    : ");
  for (i=1; i<=  p->nb_sommets; i++)
    if (p->couleur[i] != BLANC) 
      printf("%3d",p->pere[i]);
    else
      printf("  _");
  printf("\nprof    : ");
  for (i=1; i<=  p->nb_sommets; i++)
    if (p->couleur[i] != BLANC) 
      printf("%3d",p->prof[i]);
    else
      printf("  _");
  printf("\n");  
  printf("Nombre de sommets explorés : %d\n", p->nb_sommets_explores); 
}
/****************** fin structure Tparcours *********************/ 

void visiter(int s, Tgraphe *g, Tparcours *p) {
  /* Visite du sommet s */
  /* La structure p doit avoir été initialisée */
    int successeur;
    iterateur_l *i_it;
    int espace= 3 * p->prof[s], i;

    for(i=1; i<=espace; i++){
        printf(" ");
    }
    
    printf("début visiter(%d)\n", s);
    p->couleur[s]= GRIS;
    for(i_it = liste_succ(s, g); !fini_it(i_it); i_it=suivant_it(i_it) ){
        successeur= element_it(i_it);
        if(p->couleur[successeur] == BLANC){         
            p->pere[successeur]= s;
            p->prof[successeur]= 1 + p->prof[p->pere[successeur]];
            visiter(successeur, g, p);
        }
    }
    p->couleur[s]= NOIR;
    p->nb_sommets_explores++;
    
    for(i=1; i<=espace; i++){
        printf(" ");
    }
    printf("fin visiter(%d)\n", s);
    
    /* Pour obtenir l'indentation de l'affichage comme dans les fichiers de
     log (les espaces au début des lignes "début visiter" et "fin
     visiter"), il faut utiliser le tableau profondeur (le nombre d'espace
     est égal à 3 fois la profondeur du sommet).
  */
    
}

Liste *chemin(Tparcours *p, int dest) {
  /* Retourne une liste de sommets contenant le chemin de la racine de
     l'arborescence de parcours jusqu'au sommet dest. Vous pourrez ensuite
     utiliser la fonction d'affichage d'une liste (fournie dans liste.h)
     pour afficher le chemin. */
    Liste *chemin_l= creer_liste_vide();
    int sommet_courant= dest;

    /* on test si la destination a ete atteinte, si oui on remonte le chemin à l'aide du tableau pere */
    if(p->couleur[dest] == NOIR){
        while(p->pere[sommet_courant] != 0){
            empile(sommet_courant, chemin_l);
            sommet_courant= p->pere[sommet_courant];
        }
        empile(sommet_courant, chemin_l);
    }
    
    return chemin_l; // à modifier
}


int main(int argc, char **argv) {
  Tgraphe *g;
  int r; /* r est le sommet de départ du parcours (donc aussi la racine de l'arbo de parcours) */
  int dest; /* dest est l'eventuel sommet de destination */
  Tparcours *p;
  int n, i;
  Liste *chemin_r_dest=NULL;

  if (argc < 2 || argc > 3) {
    fprintf(stderr, "usage: %s racine [dest]\n", argv[0]);
    fprintf(stderr, " racine : sommet de départ du parcours\n");
    fprintf(stderr, " dest   : sommet de destination du parcours (facultatif) \n");
    exit(1);
  }

  g = lire_graphe();
  afficheTLA(g);
  printf("\n");
  r = atoi(argv[1]);
  if (r < 1 || r > nb_sommets(g)) {
    fprintf(stderr, "Erreur, le sommet de départ du parcours %d n'est pas compris entre 1 et %d\n", r, nb_sommets(g));
    exit(3);
  }
  if (argc == 3) {
    dest = atoi(argv[2]);
    if (dest < 1 || dest > nb_sommets(g)) {
      fprintf(stderr, "Erreur, le sommet de destination du parcours %d n'est pas compris entre 1 et %d\n", dest, nb_sommets(g));
      exit(3);
    }
  }

  /* à completer */
  
  // creer une struture parcours et l'initialiser:
  n= nb_sommets(g);
  p= new_parcours(nb_sommets(g));
  p->nb_sommets_explores=0;
  for(i=1; i<=n; i++){
      p->pere[i]= -1;
      p->couleur[i]= BLANC;
      p->prof[i]= -1;
  }
  p->pere[r]=0;
  p->prof[r]=0;

  // lancer le parcours:
  visiter(r, g, p);
  printf("\n");
  afficher_parcours(p);

  if(argc == 3){
      // afficher le chemin de r à dest s'il existe.
      chemin_r_dest= chemin(p, dest);

      if(est_vide(chemin_r_dest)){
          printf("Il n'y a pas de chemin de %d à %d.\n", r, dest);
      }
      else{
          printf("Chemin de %d à %d : ", r, dest); 
          affiche_liste(chemin_r_dest);
          printf("\n");
      }
  }
  
  exit(0);
}
