Tableau des listes d'adjacence :
  successeurs de 1: < 4 >
  successeurs de 2: < >
  successeurs de 3: < 4 8 20 >
  successeurs de 4: < 3 1 >
  successeurs de 5: < 8 22 >
  successeurs de 6: < >
  successeurs de 7: < >
  successeurs de 8: < 5 3 >
  successeurs de 9: < >
  successeurs de 10: < >
  successeurs de 11: < >
  successeurs de 12: < >
  successeurs de 13: < >
  successeurs de 14: < >
  successeurs de 15: < >
  successeurs de 16: < >
  successeurs de 17: < 20 22 >
  successeurs de 18: < >
  successeurs de 19: < >
  successeurs de 20: < 17 3 >
  successeurs de 21: < 22 24 >
  successeurs de 22: < 21 17 5 >
  successeurs de 23: < >
  successeurs de 24: < 21 >

début visiter(1)
   début visiter(4)
      début visiter(3)
         début visiter(8)
            début visiter(5)
               début visiter(22)
                  début visiter(21)
                     début visiter(24)
                     fin visiter(24)
                  fin visiter(21)
                  début visiter(17)
                     début visiter(20)
                     fin visiter(20)
                  fin visiter(17)
               fin visiter(22)
            fin visiter(5)
         fin visiter(8)
      fin visiter(3)
   fin visiter(4)
fin visiter(1)

Contenu des tableaux :
            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
couleur :   N  B  N  N  N  B  B  N  B  B  B  B  B  B  B  B  N  B  B  N  N  N  B  N
pere    :   0  _  4  1  8  _  _  3  _  _  _  _  _  _  _  _ 22  _  _ 17 22  5  _ 21
prof    :   0  _  2  1  4  _  _  3  _  _  _  _  _  _  _  _  6  _  _  7  6  5  _  7
Nombre de sommets explorés : 10
Chemin de 1 à 24 : < 1 4 3 8 5 22 21 24 >
