#!/bin/bash
# 
# 1er parametre : fichier contenant la liste des arcs du graphe

if (($# != 1)); then
    echo "usage : $0 liste_arcs.txt"
    echo " liste_arcs.txt est le fichier contenant la liste des arcs du graphe"
    echo " le chemin est lu sur l'entrée standard "
    exit 13
fi

cat | tr -s "[:blank:]\n" " " | sed 's/^ //' | tr -s " " "\n" | (
    read precedent;
    while read courant; do
        grep "^$precedent $courant " $1
        precedent=$courant
    done    
    ) | (dir=0; poidstot=0;
         while read a b poids cmt; do
             poidstot=$((poidstot+poids));
             if (( poids == 0 )); then
                 echo 0 $cmt
             else
                 if (( $dir==0 ));
                 then echo "$a $b 1 =>"  $cmt
                 else echo "$a $b 1 <="  $cmt
                 fi
                 dir=$((1-dir))
             fi
         done
         echo "nombre de traversées de la rivière : $poidstot"
    )         

