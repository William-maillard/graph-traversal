########## ../parcours 1 < ../graphes_exemples/ex1.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 3 4 >
  successeurs de 2: < 3 >
  successeurs de 3: < >
  successeurs de 4: < 3 >

début visiter(1)
   début visiter(2)
      début visiter(3)
      fin visiter(3)
   fin visiter(2)
   début visiter(4)
   fin visiter(4)
fin visiter(1)

Contenu des tableaux :
            1  2  3  4
couleur :   N  N  N  N
pere    :   0  1  2  1
prof    :   0  1  2  1
Nombre de sommets explorés : 4

########## ../parcours 1 < ../graphes_exemples/ex2.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 3 4 >
  successeurs de 2: < 2 3 >
  successeurs de 3: < 1 >
  successeurs de 4: < 3 >

début visiter(1)
   début visiter(2)
      début visiter(3)
      fin visiter(3)
   fin visiter(2)
   début visiter(4)
   fin visiter(4)
fin visiter(1)

Contenu des tableaux :
            1  2  3  4
couleur :   N  N  N  N
pere    :   0  1  2  1
prof    :   0  1  2  1
Nombre de sommets explorés : 4

########## ../parcours 1 < ../graphes_exemples/G1.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 5 >
  successeurs de 2: < 1 7 >
  successeurs de 3: < 2 7 >
  successeurs de 4: < 8 >
  successeurs de 5: < 6 >
  successeurs de 6: < 2 >
  successeurs de 7: < 4 6 >
  successeurs de 8: < 4 7 >

début visiter(1)
   début visiter(2)
      début visiter(7)
         début visiter(4)
            début visiter(8)
            fin visiter(8)
         fin visiter(4)
         début visiter(6)
         fin visiter(6)
      fin visiter(7)
   fin visiter(2)
   début visiter(5)
   fin visiter(5)
fin visiter(1)

Contenu des tableaux :
            1  2  3  4  5  6  7  8
couleur :   N  N  B  N  N  N  N  N
pere    :   0  1  _  7  1  7  2  4
prof    :   0  1  _  3  1  3  2  4
Nombre de sommets explorés : 7

########## ../parcours 1 < ../graphes_exemples/G2.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 3 4 >
  successeurs de 2: < >
  successeurs de 3: < 5 >
  successeurs de 4: < 2 >
  successeurs de 5: < 4 8 >
  successeurs de 6: < 1 2 >
  successeurs de 7: < 4 >
  successeurs de 8: < 7 >

début visiter(1)
   début visiter(3)
      début visiter(5)
         début visiter(4)
            début visiter(2)
            fin visiter(2)
         fin visiter(4)
         début visiter(8)
            début visiter(7)
            fin visiter(7)
         fin visiter(8)
      fin visiter(5)
   fin visiter(3)
fin visiter(1)

Contenu des tableaux :
            1  2  3  4  5  6  7  8
couleur :   N  N  N  N  N  B  N  N
pere    :   0  4  1  5  3  _  8  5
prof    :   0  4  1  3  2  _  4  3
Nombre de sommets explorés : 7

########## ../parcours 1 < ../graphes_exemples/G3.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 4 >
  successeurs de 2: < 5 6 >
  successeurs de 3: < 1 9 10 >
  successeurs de 4: < 8 >
  successeurs de 5: < 2 7 8 >
  successeurs de 6: < 1 7 >
  successeurs de 7: < >
  successeurs de 8: < 7 >
  successeurs de 9: < 4 9 10 >
  successeurs de 10: < 3 >

début visiter(1)
   début visiter(2)
      début visiter(5)
         début visiter(7)
         fin visiter(7)
         début visiter(8)
         fin visiter(8)
      fin visiter(5)
      début visiter(6)
      fin visiter(6)
   fin visiter(2)
   début visiter(4)
   fin visiter(4)
fin visiter(1)

Contenu des tableaux :
            1  2  3  4  5  6  7  8  9 10
couleur :   N  N  B  N  N  N  N  N  B  B
pere    :   0  1  _  1  2  2  5  5  _  _
prof    :   0  1  _  1  2  2  3  3  _  _
Nombre de sommets explorés : 7

########## ../parcours 1 < ../graphes_exemples/G4.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 3 >
  successeurs de 2: < 1 4 >
  successeurs de 3: < 1 4 >
  successeurs de 4: < 2 3 5 6 7 8 >
  successeurs de 5: < 4 6 >
  successeurs de 6: < 4 5 >
  successeurs de 7: < 4 8 >
  successeurs de 8: < 4 7 >

début visiter(1)
   début visiter(2)
      début visiter(4)
         début visiter(3)
         fin visiter(3)
         début visiter(5)
            début visiter(6)
            fin visiter(6)
         fin visiter(5)
         début visiter(7)
            début visiter(8)
            fin visiter(8)
         fin visiter(7)
      fin visiter(4)
   fin visiter(2)
fin visiter(1)

Contenu des tableaux :
            1  2  3  4  5  6  7  8
couleur :   N  N  N  N  N  N  N  N
pere    :   0  1  4  2  4  5  4  7
prof    :   0  1  3  2  3  4  3  4
Nombre de sommets explorés : 8

########## ../parcours 2 1 < ../graphes_exemples/ex1.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 3 4 >
  successeurs de 2: < 3 >
  successeurs de 3: < >
  successeurs de 4: < 3 >

début visiter(2)
   début visiter(3)
   fin visiter(3)
fin visiter(2)

Contenu des tableaux :
            1  2  3  4
couleur :   B  N  N  B
pere    :   _  0  2  _
prof    :   _  0  1  _
Nombre de sommets explorés : 2
Il n'y a pas de chemin de 2 à 1.

########## ../parcours 2 1 < ../graphes_exemples/ex2.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 3 4 >
  successeurs de 2: < 2 3 >
  successeurs de 3: < 1 >
  successeurs de 4: < 3 >

début visiter(2)
   début visiter(3)
      début visiter(1)
         début visiter(4)
         fin visiter(4)
      fin visiter(1)
   fin visiter(3)
fin visiter(2)

Contenu des tableaux :
            1  2  3  4
couleur :   N  N  N  N
pere    :   3  0  2  1
prof    :   2  0  1  3
Nombre de sommets explorés : 4
Chemin de 2 à 1 : < 2 3 1 >

########## ../parcours 2 4 < ../graphes_exemples/ex2.txt
Tableau des listes d'adjacence :
  successeurs de 1: < 2 3 4 >
  successeurs de 2: < 2 3 >
  successeurs de 3: < 1 >
  successeurs de 4: < 3 >

début visiter(2)
   début visiter(3)
      début visiter(1)
         début visiter(4)
         fin visiter(4)
      fin visiter(1)
   fin visiter(3)
fin visiter(2)

Contenu des tableaux :
            1  2  3  4
couleur :   N  N  N  N
pere    :   3  0  2  1
prof    :   2  0  1  3
Nombre de sommets explorés : 4
Chemin de 2 à 4 : < 2 3 1 4 >

## ../parcours 1 2 < ../graphes_exemples/ex3.txt
Chemin de 1 à 2 : < 1 2 >
## ../parcours 1 4 < ../graphes_exemples/ex3.txt
Chemin de 1 à 4 : < 1 2 3 11 14 10 20 19 22 6 23 26 15 13 9 4 >
## ../parcours 1 6 < ../graphes_exemples/ex3.txt
Chemin de 1 à 6 : < 1 2 3 11 14 10 20 19 22 6 >
## ../parcours 1 9 < ../graphes_exemples/ex3.txt
Chemin de 1 à 9 : < 1 2 3 11 14 10 20 19 22 6 23 26 15 13 9 >
## ../parcours 1 11 < ../graphes_exemples/ex3.txt
Chemin de 1 à 11 : < 1 2 3 11 >
## ../parcours 1 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 1 à 46.
## ../parcours 1 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 1 à 49.
## ../parcours 1 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 1 à 41.
## ../parcours 1 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 1 à 43.
## ../parcours 3 2 < ../graphes_exemples/ex3.txt
Chemin de 3 à 2 : < 3 11 1 2 >
## ../parcours 3 4 < ../graphes_exemples/ex3.txt
Chemin de 3 à 4 : < 3 11 1 2 8 9 4 >
## ../parcours 3 6 < ../graphes_exemples/ex3.txt
Chemin de 3 à 6 : < 3 11 1 2 8 9 4 10 20 19 22 6 >
## ../parcours 3 9 < ../graphes_exemples/ex3.txt
Chemin de 3 à 9 : < 3 11 1 2 8 9 >
## ../parcours 3 11 < ../graphes_exemples/ex3.txt
Chemin de 3 à 11 : < 3 11 >
## ../parcours 3 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 3 à 46.
## ../parcours 3 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 3 à 49.
## ../parcours 3 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 3 à 41.
## ../parcours 3 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 3 à 43.
## ../parcours 6 2 < ../graphes_exemples/ex3.txt
Chemin de 6 à 2 : < 6 14 2 >
## ../parcours 6 4 < ../graphes_exemples/ex3.txt
Chemin de 6 à 4 : < 6 14 2 3 19 22 12 26 15 13 9 4 >
## ../parcours 6 6 < ../graphes_exemples/ex3.txt
Chemin de 6 à 6 : < 6 >
## ../parcours 6 9 < ../graphes_exemples/ex3.txt
Chemin de 6 à 9 : < 6 14 2 3 19 22 12 26 15 13 9 >
## ../parcours 6 11 < ../graphes_exemples/ex3.txt
Chemin de 6 à 11 : < 6 14 2 3 11 >
## ../parcours 6 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 6 à 46.
## ../parcours 6 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 6 à 49.
## ../parcours 6 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 6 à 41.
## ../parcours 6 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 6 à 43.
## ../parcours 8 2 < ../graphes_exemples/ex3.txt
Chemin de 8 à 2 : < 8 9 4 10 20 19 22 6 14 2 >
## ../parcours 8 4 < ../graphes_exemples/ex3.txt
Chemin de 8 à 4 : < 8 9 4 >
## ../parcours 8 6 < ../graphes_exemples/ex3.txt
Chemin de 8 à 6 : < 8 9 4 10 20 19 22 6 >
## ../parcours 8 9 < ../graphes_exemples/ex3.txt
Chemin de 8 à 9 : < 8 9 >
## ../parcours 8 11 < ../graphes_exemples/ex3.txt
Chemin de 8 à 11 : < 8 9 4 10 20 19 22 6 14 2 3 11 >
## ../parcours 8 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 8 à 46.
## ../parcours 8 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 8 à 49.
## ../parcours 8 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 8 à 41.
## ../parcours 8 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 8 à 43.
## ../parcours 10 2 < ../graphes_exemples/ex3.txt
Chemin de 10 à 2 : < 10 20 19 22 6 14 2 >
## ../parcours 10 4 < ../graphes_exemples/ex3.txt
Chemin de 10 à 4 : < 10 20 19 22 6 14 2 3 26 15 13 9 4 >
## ../parcours 10 6 < ../graphes_exemples/ex3.txt
Chemin de 10 à 6 : < 10 20 19 22 6 >
## ../parcours 10 9 < ../graphes_exemples/ex3.txt
Chemin de 10 à 9 : < 10 20 19 22 6 14 2 3 26 15 13 9 >
## ../parcours 10 11 < ../graphes_exemples/ex3.txt
Chemin de 10 à 11 : < 10 20 19 22 6 14 2 3 11 >
## ../parcours 10 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 10 à 46.
## ../parcours 10 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 10 à 49.
## ../parcours 10 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 10 à 41.
## ../parcours 10 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 10 à 43.
## ../parcours 15 2 < ../graphes_exemples/ex3.txt
Chemin de 15 à 2 : < 15 13 9 4 10 20 19 22 6 14 2 >
## ../parcours 15 4 < ../graphes_exemples/ex3.txt
Chemin de 15 à 4 : < 15 13 9 4 >
## ../parcours 15 6 < ../graphes_exemples/ex3.txt
Chemin de 15 à 6 : < 15 13 9 4 10 20 19 22 6 >
## ../parcours 15 9 < ../graphes_exemples/ex3.txt
Chemin de 15 à 9 : < 15 13 9 >
## ../parcours 15 11 < ../graphes_exemples/ex3.txt
Chemin de 15 à 11 : < 15 13 9 4 10 20 19 22 6 14 2 3 11 >
## ../parcours 15 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 15 à 46.
## ../parcours 15 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 15 à 49.
## ../parcours 15 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 15 à 41.
## ../parcours 15 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 15 à 43.
## ../parcours 17 2 < ../graphes_exemples/ex3.txt
Chemin de 17 à 2 : < 17 11 1 2 >
## ../parcours 17 4 < ../graphes_exemples/ex3.txt
Chemin de 17 à 4 : < 17 11 1 2 3 19 22 6 14 13 9 4 >
## ../parcours 17 6 < ../graphes_exemples/ex3.txt
Chemin de 17 à 6 : < 17 11 1 2 3 19 22 6 >
## ../parcours 17 9 < ../graphes_exemples/ex3.txt
Chemin de 17 à 9 : < 17 11 1 2 3 19 22 6 14 13 9 >
## ../parcours 17 11 < ../graphes_exemples/ex3.txt
Chemin de 17 à 11 : < 17 11 >
## ../parcours 17 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 17 à 46.
## ../parcours 17 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 17 à 49.
## ../parcours 17 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 17 à 41.
## ../parcours 17 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 17 à 43.
## ../parcours 18 2 < ../graphes_exemples/ex3.txt
Chemin de 18 à 2 : < 18 2 >
## ../parcours 18 4 < ../graphes_exemples/ex3.txt
Chemin de 18 à 4 : < 18 2 3 11 14 10 20 19 22 6 23 26 15 13 9 4 >
## ../parcours 18 6 < ../graphes_exemples/ex3.txt
Chemin de 18 à 6 : < 18 2 3 11 14 10 20 19 22 6 >
## ../parcours 18 9 < ../graphes_exemples/ex3.txt
Chemin de 18 à 9 : < 18 2 3 11 14 10 20 19 22 6 23 26 15 13 9 >
## ../parcours 18 11 < ../graphes_exemples/ex3.txt
Chemin de 18 à 11 : < 18 2 3 11 >
## ../parcours 18 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 18 à 46.
## ../parcours 18 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 18 à 49.
## ../parcours 18 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 18 à 41.
## ../parcours 18 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 18 à 43.
## ../parcours 20 2 < ../graphes_exemples/ex3.txt
Chemin de 20 à 2 : < 20 19 22 6 14 2 >
## ../parcours 20 4 < ../graphes_exemples/ex3.txt
Chemin de 20 à 4 : < 20 19 22 6 14 2 3 26 15 13 9 4 >
## ../parcours 20 6 < ../graphes_exemples/ex3.txt
Chemin de 20 à 6 : < 20 19 22 6 >
## ../parcours 20 9 < ../graphes_exemples/ex3.txt
Chemin de 20 à 9 : < 20 19 22 6 14 2 3 26 15 13 9 >
## ../parcours 20 11 < ../graphes_exemples/ex3.txt
Chemin de 20 à 11 : < 20 19 22 6 14 2 3 11 >
## ../parcours 20 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 20 à 46.
## ../parcours 20 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 20 à 49.
## ../parcours 20 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 20 à 41.
## ../parcours 20 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 20 à 43.
## ../parcours 22 2 < ../graphes_exemples/ex3.txt
Chemin de 22 à 2 : < 22 6 14 2 >
## ../parcours 22 4 < ../graphes_exemples/ex3.txt
Chemin de 22 à 4 : < 22 6 14 2 3 26 15 13 9 4 >
## ../parcours 22 6 < ../graphes_exemples/ex3.txt
Chemin de 22 à 6 : < 22 6 >
## ../parcours 22 9 < ../graphes_exemples/ex3.txt
Chemin de 22 à 9 : < 22 6 14 2 3 26 15 13 9 >
## ../parcours 22 11 < ../graphes_exemples/ex3.txt
Chemin de 22 à 11 : < 22 6 14 2 3 11 >
## ../parcours 22 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 22 à 46.
## ../parcours 22 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 22 à 49.
## ../parcours 22 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 22 à 41.
## ../parcours 22 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 22 à 43.
## ../parcours 41 2 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 41 à 2.
## ../parcours 41 4 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 41 à 4.
## ../parcours 41 6 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 41 à 6.
## ../parcours 41 9 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 41 à 9.
## ../parcours 41 11 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 41 à 11.
## ../parcours 41 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 41 à 46.
## ../parcours 41 49 < ../graphes_exemples/ex3.txt
Chemin de 41 à 49 : < 41 31 32 33 49 >
## ../parcours 41 41 < ../graphes_exemples/ex3.txt
Chemin de 41 à 41 : < 41 >
## ../parcours 41 43 < ../graphes_exemples/ex3.txt
Chemin de 41 à 43 : < 41 31 32 33 49 52 36 44 43 >
## ../parcours 46 2 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 2.
## ../parcours 46 4 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 4.
## ../parcours 46 6 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 6.
## ../parcours 46 9 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 9.
## ../parcours 46 11 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 11.
## ../parcours 46 46 < ../graphes_exemples/ex3.txt
Chemin de 46 à 46 : < 46 >
## ../parcours 46 49 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 49.
## ../parcours 46 41 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 41.
## ../parcours 46 43 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 46 à 43.
## ../parcours 50 2 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 50 à 2.
## ../parcours 50 4 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 50 à 4.
## ../parcours 50 6 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 50 à 6.
## ../parcours 50 9 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 50 à 9.
## ../parcours 50 11 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 50 à 11.
## ../parcours 50 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 50 à 46.
## ../parcours 50 49 < ../graphes_exemples/ex3.txt
Chemin de 50 à 49 : < 50 49 >
## ../parcours 50 41 < ../graphes_exemples/ex3.txt
Chemin de 50 à 41 : < 50 49 52 36 44 32 33 41 >
## ../parcours 50 43 < ../graphes_exemples/ex3.txt
Chemin de 50 à 43 : < 50 49 52 36 44 32 33 56 45 43 >
## ../parcours 59 2 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 59 à 2.
## ../parcours 59 4 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 59 à 4.
## ../parcours 59 6 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 59 à 6.
## ../parcours 59 9 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 59 à 9.
## ../parcours 59 11 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 59 à 11.
## ../parcours 59 46 < ../graphes_exemples/ex3.txt
Il n'y a pas de chemin de 59 à 46.
## ../parcours 59 49 < ../graphes_exemples/ex3.txt
Chemin de 59 à 49 : < 59 31 32 33 41 44 40 50 49 >
## ../parcours 59 41 < ../graphes_exemples/ex3.txt
Chemin de 59 à 41 : < 59 31 32 33 41 >
## ../parcours 59 43 < ../graphes_exemples/ex3.txt
Chemin de 59 à 43 : < 59 31 32 33 41 44 40 50 49 52 36 53 56 45 43 >
