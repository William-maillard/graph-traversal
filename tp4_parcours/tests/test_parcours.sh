#!/bin/bash

####### parcours complets des graphes
### ex1
echo "########## ../parcours 1 < ../graphes_exemples/ex1.txt"
../parcours 1 < ../graphes_exemples/ex1.txt
echo

### ex2
echo "########## ../parcours 1 < ../graphes_exemples/ex2.txt"
../parcours 1 < ../graphes_exemples/ex2.txt
echo

# graphes TD
for g in G1 G2 G3 G4; do
    echo "########## ../parcours 1 < ../graphes_exemples/$g.txt"
    ../parcours 1 < ../graphes_exemples/$g.txt
    echo
done


### recherche de chemins
echo "########## ../parcours 2 1 < ../graphes_exemples/ex1.txt"
../parcours 2 1 < ../graphes_exemples/ex1.txt
echo

echo "########## ../parcours 2 1 < ../graphes_exemples/ex2.txt"
../parcours 2 1 < ../graphes_exemples/ex2.txt
echo

echo "########## ../parcours 2 4 < ../graphes_exemples/ex2.txt"
../parcours 2 4 < ../graphes_exemples/ex2.txt
echo


# "gros" graphe à 60 sommets
for deb in 1 3 6 8 10 15 17 18 20 22 41 46 50 59; do
    for fin in 2 4 6 9 11 46 49 41 43; do
        echo "## ../parcours $deb $fin < ../graphes_exemples/ex3.txt"
        ../parcours $deb $fin < ../graphes_exemples/ex3.txt | grep hemin
    done
done
