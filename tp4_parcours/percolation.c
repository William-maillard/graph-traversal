/* Les membres du groupe de TP (groupes de 2 au maximum, redoublants seuls) :
 MAILLARD William
*/
#include <stdio.h>
#include <stdlib.h>
#include "include/liste.h"
#include "include/xmalloc.h"
#include "include/affichage.h"
#include "include/graphe_TLA.h"


/****************** structure Tparcours  *************************/ 
/* cette structure sert à stocker les tableaux utilisés pendant le parcours:
  - couleur[]
  - pere[]  : pour stocker l'arborescence de parcours
  - prof[]  : la profondeur des sommets dans l'arborescence.
ainsi que:
  - nb_sommets : le nombre de sommets du graphe
  - nb_sommets_explores : le nombre de sommets explorés par le parcours.
*/
#define BLANC 1 /* valeurs possibles dans le tableau couleur */
#define GRIS 2
#define NOIR 3

typedef struct parcours {
  int nb_sommets; 
  int nb_sommets_explores; 
  int *couleur;
  int *pere;
  int *prof;
} Tparcours;

Tparcours *new_parcours(int n) {
  /* alloue la mémoire pour une structure Tparcours sur un graphe à n
     sommets et retourne un pointeur dessus. Ne remplit pas les tableaux. */
  Tparcours *p;

  p = (Tparcours*) xmalloc(sizeof(Tparcours));
  p->couleur = (int *) xmalloc((n+1)*sizeof(int));
  p->pere = (int *) xmalloc((n+1)*sizeof(int));
  p->prof = (int *) xmalloc((n+1)*sizeof(int));
  p->nb_sommets = n;
  return p;
}

void afficher_parcours(Tparcours *p) {
  /* affiche le contenu des tableaux pere, couleur et prof du parcours
     ainsi que le nombre de sommets explorés */
  int i;

  printf("Contenu des tableaux :\n");
  printf("          ");
  for (i=1; i<= p->nb_sommets; i++) 
    printf("%3d",i);
  printf("\ncouleur : ");
  for (i=1; i<=  p->nb_sommets; i++) {
    if (p->couleur[i] == BLANC) printf("  B");
    if (p->couleur[i] == NOIR) printf("  N");
    if (p->couleur[i] == GRIS) printf("  G");
  } 
  printf("\npere    : ");
  for (i=1; i<=  p->nb_sommets; i++)
    if (p->couleur[i] != BLANC) 
      printf("%3d",p->pere[i]);
    else
      printf("  _");
  printf("\nprof    : ");
  for (i=1; i<=  p->nb_sommets; i++)
    if (p->couleur[i] != BLANC) 
      printf("%3d",p->prof[i]);
    else
      printf("  _");
  printf("\n");  
  printf("Nombre de sommets explorés : %d\n", p->nb_sommets_explores); 
}

void detruit_parcours(Tparcours *p) {
    /*  Pour liberer la mémoire alloué pour le parcours p.  Utile surtout pour
    la fin du tp quand il faut générer plusieurs matrices, dans ce cas si
    on ne libere pas la mémoire une fois qu'une matrice a été parcourue, on
    risque de saturer la mémoire.
  */
  free(p->couleur);
  free(p->pere);
  free(p->prof);
  free(p);
}
/****************** fin structure Tparcours *********************/ 



/****************** structure Tmatrice *********************/ 
#define VIDE 0  /* valeurs possibles dans la matrice */
#define PLEIN 1

typedef struct matrice {
  int h; /* hauteur */
  int l; /* largeur */
  int *mat; /* chaque case du tableau contient VIDE ou PLEIN */
} Tmatrice;


Tmatrice *gen_matrice(int hauteur, int largeur, double proba) {
  /* retourne un pointeur sur une matrice générée aléatoirement */
  /* proba est la probabilité qu'une case de la matrice soit vide. */
  Tmatrice *m;
  int i;
  
  m = (Tmatrice *) xmalloc(sizeof(Tmatrice));
  m->h = hauteur;
  m->l = largeur;
  m->mat = (int *) xmalloc((largeur*hauteur+1)*sizeof(int));
  for (i = 1; i <= largeur*hauteur; i++) {
    if ((double)random()/RAND_MAX < proba) 
      m->mat[i] = VIDE;
    else
      m->mat[i] = PLEIN;
  }
  return m;
}

void detruit_matrice(Tmatrice *m) {
  /*  Pour liberer la mémoire alloué pour la matrice m.  Utile surtout pour
    la fin du tp quand il faut générer plusieurs matrices, dans ce cas si
    on ne libere pas la mémoire une fois qu'une matrice a été parcourue, on
    risque de saturer la mémoire.
  */
  free(m->mat);
  free(m);
}
/****************** fin structure Tmatrice *********************/ 


/****************** Affichage *********************/ 

/* Couleurs utilisées pour l'affichage (cf affichage.h) */
#define COUL_BLANC coul_gris(23)
#define COUL_GRIS coul_gris(14) //coul_rvb(2,2,3) 
#define COUL_NOIR coul_gris(5)
#define COUL_PLEIN coul_rvb(3,2,1)

void affiche_legende_couleurs() {
  /* affiche la legende des couleurs, comme son nom l'indique */
  printf("Legende :\n");
  printf(" Case pleine : "); couleur_fond(COUL_PLEIN); printf(" "); 
  reset_couleurs();
  printf("     Case vide non explorée : "); couleur_fond(COUL_BLANC); 
  printf(" "); reset_couleurs();
  printf("\n Cases explorées : "); 
  printf(" NOIR : "); couleur_fond(COUL_NOIR); printf(" "); reset_couleurs();
  printf("    GRIS : "); couleur_fond(COUL_GRIS); printf(" "); reset_couleurs();
  printf("\n");
}

void affiche_mat(Tmatrice *m, Tparcours *p) {
  /* Si p == NULL, affiche juste la matrice m à l'écran.
     Si p != NULL, fait l'affichage en utilisant des couleurs differentes
     pour les sommets selon la valeur de p->couleur. Pour afficher la
     legende des couleurs, utiliser la fonction affiche_legende_couleurs
     ci-dessus. */
  int i, j, numero_sommet;
  int h = m->h;
  int l = m->l;
  
  numero_sommet = 1;
  for (j = 1; j <= h; j++) {
    for (i = 1; i <= l; i++) {
      /* choix de la couleur de fond */
      if (m->mat[numero_sommet] == PLEIN) {
	couleur_fond(COUL_PLEIN);
      } else if (p == NULL) {
	couleur_fond(COUL_BLANC);
      } else {
	if (p->couleur[numero_sommet] == BLANC) couleur_fond(COUL_BLANC);
	if (p->couleur[numero_sommet] == GRIS)  couleur_fond(COUL_GRIS);
	if (p->couleur[numero_sommet] == NOIR)  couleur_fond(COUL_NOIR);
      }
      printf(" ");
      numero_sommet++;
    }
    reset_couleurs();
    printf("\n");
  }
  printf("\n");
}
/****************** fin affichage *********************/ 

Tgraphe *gen_graphe(Tmatrice *m) {
  /* Retourne le graphe correspondant à la matrice passée en paramètre. */
  Tgraphe *g;
  int n, i, ligne_courante=1, voisin;
  int l=m->l, h=m->h;

  g = creer_graphe(m->h * m->l); // crée un graphe de la même taille que la matrice.
  n= nb_sommets(g);
  
  /* à completer. Il faut maintenant rajouter les arêtes du graphe.  

     Il faut avoir bien compris comment le graphe est construit à partir de
     la matrice (voir l'enoncé). */

  for(i=1; i<=n; i++){
      /* change on de ligne ? */
      if(i > l*ligne_courante){
          ligne_courante++;
      }
      /* si la case est "PLEIN" pas d'arc à rajouter, sinon :*/
      if(m->mat[i] != PLEIN){
          /* On regarde la case "au dessus", si pas à la première ligne */
          if(ligne_courante != 1){
              voisin= i- l;
              if(m->mat[voisin] == VIDE){
                  ajoute_arc(i, voisin, g);
              }
          }
          /* On regarde la case à droite, si pas à la fin d'une ligne */
          if(i != l * ligne_courante ){
              voisin= i + 1;
              if(m->mat[voisin] == VIDE){
                  ajoute_arc(i, voisin, g);
              }
          }
          /* On regarde la case "en bas", si pas à la dernière ligne */
          if(ligne_courante != h){
              voisin= i + l;
              if(m->mat[voisin] == VIDE){
                  ajoute_arc(i, voisin, g);
              }
          }
          /* On regarde la case à gauche, si pas au début d'une ligne */
          if(i != (l* (ligne_courante-1)) + 1){
              voisin= i - 1;
              if(m->mat[voisin] == VIDE){
                  ajoute_arc(i, voisin, g);
              }
          } 
      }
  }
  
  return g;
}




void visiter(int s, Tgraphe *g, Tparcours *p, Tmatrice *m) {
    /* Visite le sommet s. Idem programme parcours */
    /* Le paramètre m ne sert que si on veut afficher la matrice pendant le
       parcours (pour faire une sorte d'animation du parcours) */
    /* ..... */
    /* Visite du sommet s */
    /* La structure p doit avoir été initialisée */
    int successeur;
    iterateur_l *i_it;
    /*int espace= 3 * p->prof[s], i;*/
    
    /*
    for(i=1; i<=espace; i++){
        printf(" ");
    }
     printf("début visiter(%d)\n", s);
    */
    p->couleur[s]= GRIS;

    /* Si on veut afficher les matrices étapes par étapes */
    if(m != NULL){
        affiche_mat(m, p);
    }
    
    for(i_it = liste_succ(s, g); !fini_it(i_it); i_it=suivant_it(i_it) ){
        successeur= element_it(i_it);
        if(p->couleur[successeur] == BLANC){         
            p->pere[successeur]= s;
            p->prof[successeur]= 1 + p->prof[p->pere[successeur]];
            visiter(successeur, g, p, m);
        }
    }
    p->couleur[s]= NOIR;
    p->nb_sommets_explores++;

    /*
    for(i=1; i<=espace; i++){
        printf(" ");
    }
    printf("fin visiter(%d)\n", s);
    */
}


int percolation(Tgraphe *g, Tmatrice *m) {
  /* retourne 1 s'il y a un chemin de la 1ere à la derniere ligne de la
     matrice et 0 sinon */
  /* utilise certainement la fonction visiter... */  
  /* la matrice m est passée en paramètre uniquement pour l'affichage */
    /* initialisation du parcours */
    int i, chemin=0, n=nb_sommets(g);
    int h= m->h, l= m->l, max;
    Tparcours *p=NULL;
    
    p= new_parcours(nb_sommets(g));
    p->nb_sommets_explores=0;
    for(i=1; i<=n; i++){
        p->pere[i]= -1;
        p->couleur[i]= BLANC;
        p->prof[i]= -1;
    }

    /* On lance des parcours à partir des sommets de la première ligne, s'il n'ont pas été visités */
    for(i=1; i<=l; i++){
        if(p->couleur[i] == BLANC){
            p->pere[i]=0;
            p->prof[i]=0;
            visiter(i, g, p, NULL);
                }
    }

    printf("\nExploration finie :\n");
    affiche_mat(m, p);
    
    /* On regarde si un sommets de la dernière ligne à été atteint */
    i= (h -1) * l +1;
    max= h * l;
    while(chemin == 0  &&  i<=max){
        if(p->couleur[i] == NOIR){
            chemin=1;
        }
        i++;
    }

    detruit_parcours(p); // on ne s'en sert que dans la fonction.
    
  return chemin;
}


int main(int argc, char **argv) {
    int h, l;
    double proba, proportion;
    Tmatrice *m=NULL;
    Tgraphe *g=NULL;
    int chemin;
    int i, nb_chemins=0;

    if (argc != 4) {
        fprintf(stderr, "Il faut 3 paramètres : hauteur largeur proba\n");
        exit(1);
    }
    h = atoi(argv[1]);
    l = atoi(argv[2]);
    proba = atof(argv[3]);
    srandom(0);
    printf("hauteur = %d,  largeur = %d,  proba = %f\n", h, l, proba);
    /* ......... */

    for(i=0; i<100; i++){
        
        /* Créationde la matrice */
        m= gen_matrice(h, l, proba);


        /* Génération du graphe */
        g= gen_graphe(m);

        /* affichage */
        /*afficheTLA(g);*/
        affiche_legende_couleurs();
        affiche_mat(m, NULL);

        /* recherche  de chemins */
        chemin= percolation(g, m);
        if(chemin == 0){
            printf("Il n'y a pas de chemin\n");
        }
        else{
            printf("Il y a un chemin\n");
            nb_chemins++;
        }

        /* libération de la mémoire */
        detruit_graphe(g);
        detruit_matrice(m);

    }
    printf("%d matrices crées\n", i);
    printf("nombre de chemins trouvés : %d\n", nb_chemins);
    /* affichage des resultats : proba proportion; dans le canal stderr */
    proportion= (float)nb_chemins/100.;
    fprintf(stderr,"%lf %lf\n", proba, proportion);
    
    exit(0);
}

/*
Résultats obtenus pour 100 matrices de taille 50*50 :
p=0.593000 proportion=0.490000
p=0.594000 proportion=0.510000

Résultats obtenus pour 100 matrices de taille 100*100 :
p=0.590000 proportion=0.500000

*/