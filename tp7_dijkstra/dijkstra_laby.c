/* Les membres du groupe de TP (groupes de 2 au maximum, redoublants seuls) :
MAILLARD William
*/

/* algo de dijkstra */
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include "include/xmalloc.h"
#include "include/affichage.h"
#include "include/fileprio.h" /* contient les fonctions sur les files de priorité, les distances des sommets sont dans la file */
#include "include/liste.h" /* contient les fonctions sur les listes */
#include "include/graphe_TLA.h" /* fonctions pour manipuler le graphe */
#include "include/parcours.h" /* la structure Tparcours qui contient entre
				 autre le tableau couleur[], pere[] ...*/
#include "include/laby.h" /* la structure Tlaby. Les fonctions d'affichage
			     qui y sont définies vous seront utiles.*/


    
void milli_sleep(int n) {
  /* un appel à cette fonction met le processus en sommeil pendant n
     millisecondes, vous pouvez l'utiliser pour ralentir l'affichage */
  struct timespec t;
  t.tv_sec = 0; 
  t.tv_nsec = 1000000*n; 
  nanosleep(&t,&t);
}

Tgraphe *gen_graphe(Tlaby *laby) {
  /* Retourne le graphe correspondant au labyrinthe passé en paramètre. */
  Tgraphe *g;
  int i;
  int h = laby->haut;
  int l = laby->larg;

  g = creer_graphe(h * l);  
  for (i = 1; i < (h-1)*l; i++) {
    if (laby->matrice[i] != PLEIN) {
      if (laby->matrice[i+1] != PLEIN) {
	/* arete horizontale */
	ajoute_arc(i,i+1,g);
	ajoute_arc(i+1,i,g);
      }
      if (laby->matrice[i+l] != PLEIN) {
	/* arete verticale */
	ajoute_arc(i,i+l,g);
	ajoute_arc(i+l,i,g);
      }
    }
  }
  return g;
}
  

void relaxation(Fprio *F, int x, int y, Tlaby *laby, Tparcours *p) {
    /* effectue une relaxation par rapport à l'arete (x,y).*/
    /* cette fonction est utilisée par la fonction dijkstra */
    /* poids d'une arete (i,j): somme des valeurs associées aux sommets i et j (laby->matrice[i] et laby->matrice[j]) */
    int poids_arete= laby->matrice[x] + laby->matrice[y];
    int poids_chemin_passant_par_x= valeur(F, x) + poids_arete;

    
    /*--si le poids du chemin de s0 à y passant par x est inférieur au poid du chemin actuel, on change le père et le poids du chemin de y--*/
    if( poids_chemin_passant_par_x  <  valeur(F, y) )
    {
        insere_ou_modif_val(F, y, poids_chemin_passant_par_x);
        p->pere[y]= x;
    }
}

/* *** fonction pour gérer l'animation *** */
void animation(Tparcours *p, Tlaby *laby, int s)
{
    if(p->animation == 1){
        if(p->attente > 0){
            milli_sleep(p->attente);
        }
        else if(p->attente == -1){
            while(getchar() != '\n')
                ;
        }
        reaffiche_sommet(laby, p, s);
    }
}

int dijkstra(Tgraphe *g, Tparcours *p, Tlaby *laby, int s) {
    /* Calcule le chemin de poids min de s à laby->fin (i.e., la distance pondérée).
       Cette fonction doit renvoyer le poids du chemin trouvé de s à laby->fin. 
       Si le sommet fin n'est pas accessible, renvoie -1. */
    Fprio *F; // la file de priorité contenant les prochains sommets à explorer.
    int x, y; // sommets
    iterateur_l *it_i;
  
    F = creer_fprio(nb_sommets(g)); // crée la file et initialise les valeurs à INT_MAX
    p->pere[s] = -1; // racine de l'arborescence
    insere(F, s, 0);
    p->couleur[s] = GRIS;
    animation(p, laby, s);
    


    /*--parcours des sommets tant qu'il y en a dans la liste et qu'on est pas arrivé à la fin--*/
    while( !vide_fprio(F) && p->couleur[laby->fin] != NOIR )
    {
        x= extraire_min(F);

        for( it_i= liste_succ(x, g) ; !fini_it(it_i) ; it_i= suivant_it(it_i) )
        {
            y= element_it(it_i);
            relaxation(F, x, y, laby, p);
          
            if(p->couleur[y] == BLANC)
            {
                p->couleur[y]= GRIS;
            }
          
            animation(p, laby, y);
        }

        /*--on a finis d'explorer x--*/
        p->nb_sommets_explores++;
        p->couleur[x]= NOIR;
        animation(p, laby, x);
    }
  
    /*--si on n'est pas arrivé au sommet de fin--*/
    if(p->couleur[laby->fin] == BLANC)
    {
        return -1;
    }
  
    /*--sinon, on retourne le poids du chemin trouvé--*/
    return valeur(F, laby->fin);
}

int trace_chemin(Tparcours *p, int s) {
  /* Met la valeur CHEMIN (définie dans parcours.h) dans les cases de
     p->couleur[] par lesquelles passe le chemin de l'arborescence de
     parcours allant de la racine à s et renvoie la longueur de celui-ci.

     On suppose que p->pere contient l'arborescence de parcours et que
     p->pere[racine] vaut -1.*/
    int sommet= p->pere[s], longueur=0;

    while(sommet != -1){
        p->couleur[sommet]= CHEMIN;
        sommet= p->pere[sommet];
        longueur++;
    }
    
  return longueur;
}


void usage(int argc, char **argv) {
  fprintf(stderr,         "   %s [Options] -f <laby>\n",argv[0]);
  
  fprintf(stderr,"Le labyrinthe est lu dans le fichier <laby>. Fait un parcours avec l'algo de Dijkstra\n");
    
  fprintf(stderr,"Options :\n");
  fprintf(stderr,"  -a   : fait une animation du parcours ;\n");
  fprintf(stderr,"  -w n : attend n ms avant chaque réaffichage du laby lors d'une animation;\n");
  fprintf(stderr,"  -n   : pas d'affichage du labyrinthe (incompatible avec l'option -a) ;\n");
  exit(1);
}

int main(int argc, char **argv) {
  FILE *fichier;
  char *nom_fichier;
  Tgraphe *g;
  Tparcours *p;
  Tlaby *laby;
  int depart, long_chemin, poids_chemin;
  int c;
  int no_print, animation, attente;

  /* ************ traitement des paramètres de la ligne de commande *****/
  /* comprendre ce qui se passe ici n'est pas indispensable */
  /* pour avoir des infos sur getopt : "man 3 getopt" ou google...*/
  no_print = animation = attente = 0;
  while ((c = getopt(argc, argv, "f:naw:")) != -1)
    switch (c) {
    case 'a':
      animation = 1;
      break;
    case 'w':
      attente = atoi(optarg);
      break;
    case 'n':
      no_print = 1;
      break;
    case 'f':
      nom_fichier = optarg;
      break;
    default:
      usage(argc, argv);
    }
  if (animation && no_print) usage(argc, argv);
  /* ************ fin traitement des paramètres   *************/

  /************** lecture du labyrinthe dans un fichier ********/
  if (argc>1 && optind == argc) {
    fichier = fopen(nom_fichier,"r");
    if (fichier == NULL) {
      fprintf(stderr,"Pb lors de la lecture du fichier %s\n", nom_fichier);
      exit(1);
    }
    laby = lecture_laby(fichier);
    printf("Labyrinthe : largeur = %d, hauteur = %d\n", 
	   laby->larg, laby->haut);
  }
  else
    usage(argc, argv);
  /************** fin génération ou lecture du labyrinthe ********/

  /* affichage labyrinthe */
  p = creer_parcours_vide(laby->haut * laby->larg);
  if ( ! no_print ) {
    affiche_legende_couleurs();
    affiche_laby(laby, p);
  }

  /***************** creation du graphe à partir du laby *************/
  g = gen_graphe(laby);
  /************************** fin creation graphe *******************/

  /* on prépare le parcours */
  p->animation = animation;
  p->attente = attente;
  depart = laby->larg + 2; /* sommet de depart : 2e sommet de la 2e ligne */

  /* on lance le parcours */
  poids_chemin = dijkstra(g, p, laby, depart);

  /* on trace le chemin dans p->couleur */
  if (poids_chemin >= 0) 
    long_chemin = trace_chemin(p, laby->fin);
  
  /* affichage du résultat */
  if ( ! no_print ) {
    printf("Graphe après parcours :\n");
    affiche_laby(laby, p);
    printf("\n");
  }
  if (poids_chemin >= 0)  
    printf("Longueur du chemin trouvé (poids) : %d (%d)\n", long_chemin, poids_chemin);
  else
    printf("Pas de chemin\n");
  printf("Nombre de sommets explorés : %d\n", p->nb_sommets_explores);
  return 0;
}
