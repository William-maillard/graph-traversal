#ifndef PRIOQ_H
#define PRIOQ_H

typedef struct fileprio Fprio;

/*
  Le type Fprio fournit une file à priorité et un tableau de valeurs.
  A chaque sommet est associé une valeur (aussi appelée priorité).
  La valeur de chaque sommet est stocké dans cette structure (y compris ceux qui ne sont pas dans la file).
  Les valeurs de tous les sommets sont initialisés à INT_MAX/2 lors de la création de la file.
  La valeur d'un sommet qui a été retiré de la liste est toujours accessible avec la fonction valeur().
 */


Fprio *creer_fprio(int nbe_max);
// Crée et retourne une file de priorité vide pouvant contenir jusqu'à nbe_max
// elements numérotés de 1 à nbe_max. Les valeurs des sommets sont initialisés
// à INT_MAX/2.

int dans_fprio(Fprio *fq, int s);
// retourne vrai si s est dans fp; on doit avoir 0 < s <= nbe_max

void insere_ou_modif_val(Fprio *fp, int s, int v);
// si s est deja dans file fp : met à jour sa valeur à v
// sinon rajoute s dans la file avec comme valeur v
// on doit avoir 0 < s <= nbe_max

void modif_val(Fprio *fp, int s, int nouvelle_valeur);
// modifie la valeur de s; s doit être dans la file fp (sinon utiliser insere_ou_modif_val()).

void insere(Fprio *fp, int s, int v);
// insere un nouveau sommet s de valeur v dans la file fp; on doit avoir 0 < s <= nbe_max.

int extraire_min(Fprio *fp);
// retourne le sommet de valeur min dans fp (et le supprime de p, sa valeur
// sera toujours accessible avec la fonction "valeur()")

int valeur(Fprio *fp, int s);
// Retourne la valeur du sommet s. 
// Cette fonction peut toujours etre utilisée meme si le sommet n'est pas dans la file.

int vide_fprio(Fprio *fp);
// retourne vrai si la file est vide.

long nb_perm();
/* renvoie le nombre d'opérations faites dans la file de priorité (pour
   faire des stats sur la complexité) */

#endif
