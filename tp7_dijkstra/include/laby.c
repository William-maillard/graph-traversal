#include <stdio.h>
#include <stdlib.h>
#include "xmalloc.h"
#include "liste.h"
#include "affichage.h"
#include "parcours.h"
#include "laby.h"

/* Couleurs utilisées par l'affichage.
   Définies dans lecture_laby
   le sommet i sera affiché avec la couleur de fond 
   couleur[c] ou c = laby->matrice[i] 
*/
static int couleurs[10];

void init_couleurs_defaut() {
  int i;
  // couleurs par défaut (si pas spécifiées dans le fichier du labyrinthe)
  couleurs[0] = COUL_PLEIN;  // PLEIN vaut 0
  for (i=1; i<10; i++) {
    couleurs[i] = coul_gris(21-2*i); 
    couleurs[i] = coul_gris(19-i); 
  }
}

/* ****************************************************************/
/* ********************** fonctions de base  **********************/
/* ****************************************************************/

Tlaby *creer_laby_vide(int largeur, int hauteur) {
  /* Alloue la memoire pour stocker le labyrinthe. 
     Ne fait aucune initialisation. */
  Tlaby *l;

  l = (Tlaby *) xmalloc(sizeof(struct laby));
  l->matrice = (int *) xmalloc((hauteur * largeur + 1) * sizeof(int));
  return l;
}


Tlaby *lecture_laby(FILE *f){
  /* Lit un labyrinthe dans le fichier f et le renvoie. */
  Tlaby *laby;
  int i, j,k, numero_sommet, caractere_lu, largeur, hauteur;

  init_couleurs_defaut();
  fscanf(f, "%d %d", &hauteur, &largeur);
  getc(f); /* pour consommer le \n à la fin de la 1ere ligne*/

  laby = creer_laby_vide(hauteur,largeur);
  laby->larg = largeur;
  laby->haut = hauteur;
  numero_sommet = 1;
  for (j = 1; j <= laby->haut; j++) {
    for (i = 1; i <= laby->larg; i++) {
      caractere_lu = getc(f);
      switch (caractere_lu) {
	case 'X':
	  laby->fin = numero_sommet;
	case ' ':
	  laby->matrice[numero_sommet] = 1;
	  break;
	case '#':
	  laby->matrice[numero_sommet] = PLEIN;
	  break;
	default:
	  k = caractere_lu - '0';
	  if (k>9 || k<0) {fprintf(stderr,"caractere invalide (%d,%d)  %c\n", i,j,caractere_lu); exit(2);}
	  laby->matrice[numero_sommet] = k;
      }
      numero_sommet++;
    }
    getc(f); /* consomme le \n à la fin de la ligne */
  }
  return laby;
}


/* **************************************************** */
/* **************** Affichage ************************* */
/* **************************************************** */


void affiche_sommet(Tlaby *laby, Tparcours *p, int numero_sommet) {
  /* Le curseur doit être au bon endroit */
  /* version pour des poids sur les sommets */
  /* affiche un gris plus ou moins foncé en fonction de matrice[numero_sommet] */
  /* affiche un "+" pour les sommets explorés */
  char c=' ';

  if (laby->matrice[numero_sommet] == PLEIN) {
    couleur_fond(couleurs[PLEIN]);
    c = ' ';
  }
  else {
    // couleur du texte et caractere
    if (numero_sommet == laby->fin) {
      couleur_texte(0);
      c = 'X';
    } else {
      switch (p->couleur[numero_sommet]) {
      case BLANC:
	c =  ' ';
	break;
      case GRIS:
	c = '+';
	couleur_texte(COUL_GRIS);
	break;
      case NOIR:
	c = '+';
	couleur_texte(COUL_NOIR);
	break;
      case CHEMIN:
	c = '+';
	couleur_texte(COUL_CHEMIN);
	break;
      }
    }
    // couleur du fond
    couleur_fond(couleurs[laby->matrice[numero_sommet]]);
  }
  printf("%c", c);
}

void affiche_legende_couleurs() {
  /* affiche la legende des couleurs, comme son nom l'indique */
  printf("Legende :\n");
  printf(" Case pleine : "); couleur_fond(COUL_PLEIN); printf(" "); 
  reset_couleurs();
  printf("\n Cases explorées : "); 
  printf(" NOIR : "); couleur_texte(COUL_NOIR); printf("+"); reset_couleurs();
  printf("    GRIS : "); couleur_texte(COUL_GRIS); printf("+"); reset_couleurs();
  printf("    CHEMIN : "); couleur_texte(COUL_CHEMIN); printf("+"); 
  reset_couleurs();
  printf("\n");
}

void affiche_laby(Tlaby *laby, Tparcours *p) {
  /* Affiche le labyrinthe à l'écran, utilise les fonctions de
     affiche.h pour changer la couleur. */
  int i, j, numero_sommet;

  numero_sommet = 1;
  for (j = 1; j <= laby->haut; j++) {
    for (i = 1; i <= laby->larg; i++)
      affiche_sommet(laby, p, numero_sommet++);
    reset_couleurs();
    printf("\n");
  }
}


void reaffiche_sommet(Tlaby *laby, Tparcours *p, int numero_sommet) {
/* Réaffiche le sommet numero_sommet.  
   On suppose que la matrice a deja été affiché et que le curseur se trouve
   sur la 1ere colonne de la ligne suivant la derniere ligne de la matrice.
   Replace le curseur à cet endroit.*/
  int ligne, col;
  int dep_ligne, dep_col;
  
  ligne = (numero_sommet-1) / laby->larg + 1;
  col = (numero_sommet-1) % laby->larg + 1;
  dep_ligne = laby->haut + 1 - ligne;
  dep_col = col -1;
  /* deplace le curseur au bon endroit*/
  goto_up(dep_ligne); 
  goto_forward(dep_col);
  /* affiche le sommet */
  affiche_sommet(laby, p, numero_sommet);
  /* replace le curseur */
  reset_couleurs();
  goto_down(dep_ligne-1); 
  printf("\n"); /* place le curseur au debut de la ligne suivante */
}



