// vetud_begin %%
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include "affichage.h"

// https://en.wikipedia.org/wiki/ANSI_escape_code

int coul_rvb(int r, int v, int b) {
  // valeurs r,v et b entre 0 et 5.
  return 16+36*r+6*v+b;
}

int coul_gris(int n) {
  // niveau de gris n entre 0 et 23
  return 232+n;
}

void couleur_fond(int coul) {
  printf("\033[48;5;%dm",coul);
  //printf("\033[4%dm", coul);
}

void couleur_texte(int coul) {
  printf("\033[38;5;%dm",coul);
  //printf("\033[3%dm", coul);
}

void reset_couleurs() {
  printf("\033[0m");
}

void efface_ecran() {
  printf("\033[2J");
}

/* Deplacement du curseur */

void goto_haut_gauche() {
  printf("\033[1;1H");
}

void goto_xy(int x, int y) {
  printf("\033[%d;%dH", x, y);
}

void goto_up(int x) {
  printf("\033[%dA", x);
}

void goto_down(int x) {
  printf("\033[%dB", x);
}

void goto_forward(int x) {
  printf("\033[%dC", x);
}

void goto_backward(int x) {
  printf("\033[%dD", x);
}

void save_cursor_pos() {
  printf("\033[s");
}

void restore_cursor_pos() {
  printf("\033[u");
}

// vetud_end %%
//static char magic[]=" xqmxxltqjqvkuwn";

static char magic[][8]={" ","│","─","└","│","│","┌","├","─","┘","─","┴","┐","┤","┬","┼"};


void affiche_symbole(int numero_symbole) {
  //  printf("\033(0"); // passage mode graphique (ne semble marcher que dans xterm)
  printf("%s",magic[numero_symbole]);
  //printf("\033(B"); // retour mode normal
}

/* Ces fonctions ne font pas d'affichage, mais je ne savais pas ou les
   mettre.  Normalement, l'entrée d'un processus est bufferisé (ie, le
   processus ne reçoit pas les caractères saisis avant un retour à la
   ligne). la fonction suivante sert à desactiver le buffer pour
   l'entrée. Le processus recevra donc chaque caractère dès qu'il est tappé
   par l'utilisateur.
*/


struct termios saved_attributes;
     
void reset_input_mode (void)
{
  tcsetattr (STDIN_FILENO, TCSANOW, &saved_attributes);
}
     
void set_input_mode (void)
{
  struct termios tattr;
     
  /* Make sure stdin is a terminal. */
  if (!isatty (STDIN_FILENO))
    {
      fprintf (stderr, "Not a terminal.\n");
      exit (EXIT_FAILURE);
    }
     
  /* Save the terminal attributes so we can restore them later. */
  tcgetattr (STDIN_FILENO, &saved_attributes);
  atexit (reset_input_mode);
     
  /* Set the funny terminal modes. */
  tcgetattr (STDIN_FILENO, &tattr);
  tattr.c_lflag &= ~(ICANON|ECHO); /* Clear ICANON and ECHO. */
  tattr.c_cc[VMIN] = 1;
  tattr.c_cc[VTIME] = 0;
  tcsetattr (STDIN_FILENO, TCSAFLUSH, &tattr);
}
     
