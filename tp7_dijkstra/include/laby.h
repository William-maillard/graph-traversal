#ifndef LABY_H
#define LABY_H

/* ****************************************************************/
/********************* Type Tlaby *******************************/
/* ****************************************************************/

/* Valeurs possibles pour les cases du tableau matrice */
#define PLEIN 0
/* les autres cases contiennent un entier entre 1 et 9 pour indiquer
   le temps pour traverser la case */
 
typedef struct laby{
  /* - Les cases sont numérotés à partir de 1 (et pas 0). 
     - larg et haut sont la largeur et la hauteur totale du labyrinthe, y
     compris la premiere et derniere ligne et la premiere et derniere
     colonne dont toutes les cases valent PLEIN. La taille "utile" de la
     matrice est donc (larg-2) colonnes et (haut-2) lignes */
  int larg; /* largeur matrice */
  int haut; /* hauteur matrice */
  int fin; /* numero du sommet à atteindre */
  int * matrice; /* Matrice à une dimension contenant le labyrinthe. */
} Tlaby;


Tlaby *genere_laby_aleatoire(double p, int l, int h);
/* Genere et retourne un labyrinthe aléatoire */

Tlaby *lecture_laby(FILE *f);
/* Lit un labyrinthe dans le fichier f et renvoie le labyrinthe qui
   le represente. */

/* **************************************************** */
/* **************** Affichage ************************* */
/* **************************************************** */

/* Couleurs utilisées pour l'affichage */
#define COUL_BLANC 7
#define COUL_GRIS 3 
#define COUL_NOIR 0
#define COUL_CHEMIN coul_rvb(0,0,5)
#define COUL_PLEIN coul_rvb(3,2,1)

void affiche_legende_couleurs();
/* affiche la legende des couleurs */
 
void affiche_laby(Tlaby *laby, Tparcours *p);
/* Affiche le labyrinthe à l'écran */

void reaffiche_sommet(Tlaby *laby, Tparcours *p, int numero_sommet);
/* Réaffiche le sommet numero_sommet.  
   On suppose que le labyrinthe a deja été affiché et que le curseur se trouve
   sur la 1ere colonne de la ligne suivant la derniere ligne du labyrinthe.
   Replace le curseur à cet endroit.*/

#endif
