#ifndef PARCOURS_H 
#define PARCOURS_H

/****************************************************************************/
/*******************   fichier parcours.h ***********************************/
/****************************************************************************/

/* valeurs du tableau couleur */
#define BLANC 1
#define GRIS 2
#define NOIR 3
#define CHEMIN 4 /* pour marquer les cases du chemin trouvé par l'algorithme 
		    (fonction trace_chemin) */

/* types d'algos possibles (dans le champ "algo") */
#define PROFONDEUR     1
#define RECURSIF       2
#define LARGEUR        3

typedef struct parcours{
  /* Les sommets sont numérotés à partir de 1 */
  int nb_sommets;/* nb de sommets du graphe */
  int nb_sommets_explores; /* nb de sommets explores */
  int * couleur; /* à initialiser à BLANC */
  int * pere;    /* initialiser  pere[racine] à -1 */
  int algo;      /* type d'algo à utiliser pour le parcours (cf plus haut) */
  int animation; /* 0 : pas d'animation du parcours. */
                 /* 1 : animation : votre fonction  doit redessiner
		    le graphe à chaque modification de pere[] ou couleur[] */
  int attente;   /* 0  : pas d'attente avant de redessiner le graphe;*/
                 /* > 0 : temps d'attente en ms avant de
		    redessiner */
} Tparcours;


Tparcours *creer_parcours_vide(int nb_sommets);
/* Alloue la memoire pour stocker les tableaux couleur et pere. 
   Initialise couleur à BLANC.
   Initialise p->nb_sommets et nb_sommets_explores.
   fonction appelée dans main()
*/


#endif
