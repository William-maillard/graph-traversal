/* Les membres du groupe de TP (groupes de 2 au maximum, redoublants seuls) :
MAILLARD William
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
  
/*************************************************/
/****************** GRAPHE ***********************/
/*************************************************/

/* Graphe non orienté pondéré. 
   Les sommets sont numérotés de 1 à n.
   Chaque sommet représente une ville (avec son nom et ses coordonnées x et y).
   Le poids d'une arête est défini comme la distance euclidienne entre les villes.
 */

/* Type pour les aretes du graphe.*/
/* Comme le graphe est non orienté, l'arête (10,3) peut être représentée
   soit avec a=10 et b=3 ou avec a=3 et b=10 (mais pas les deux). */
typedef struct {
  int a; /* numéro du premier sommet extremité de l'arete */
  int b; /* numéro du deuxieme sommet extremité de l'arete */
  double poids; /* poids de l'arete (distance entre les villes), calculé
		   avec la fonction poids() définie plus bas. */
} Tarete;


typedef struct str_graphe{
  /****  SOMMETS : chaque sommet représente une ville. */
  int n; /* nombre de sommets du graphe. Les sommets sont numérotés de 1 à n. */
  char **ville; /* tableau contenant les noms des villes */
  int *xcoord;  /* tableaux avec les coordonnées des villes */
  int *ycoord;

  /****  ARETES */
  int m; /* nombre d'aretes du graphe */
  /* Tableau des aretes. Les aretes sont numérotées de 0 à m-1. */
  Tarete *arete; 
} Tgraphe;


/*************************************************/
/************** Entrées/Sorties ******************/
/*************************************************/

/* Lit la liste des villes dans un fichier et crée et retourne le graphe
   correspondant (sans aucune arête).

   Format fichier d'entrée (voir dans le répertoire ex_graphes/ ):
      Premiere ligne du fichier : nombre de villes,
      Ensuite chaque ville est décrite sur deux lignes :
        nom_ville
        coord_x coord_y
*/
Tgraphe *lire_graphe(char *nom_fichier) {
  int i;
  Tgraphe *g;
  FILE *fichier;
  char *ligne;
  int l;
  size_t n;

  fprintf(stderr,"Lecture du fichier %s\n", nom_fichier);
  fichier = fopen(nom_fichier, "r");
  if (fichier == NULL) {
    fprintf(stderr,"Impossible d'ouvrir le fichier %s\n", nom_fichier);
    exit(2);
  }
  
  g = malloc(sizeof(Tgraphe));
  
  fscanf(fichier, " %d ", &(g->n));
  fprintf(stderr,"Nombre de villes %d\n", g->n);
  g->ville = malloc(sizeof(char *) * (g->n+1));
  g->xcoord = malloc(sizeof(int) * (g->n+1));
  g->ycoord = malloc(sizeof(int) * (g->n+1));
  g->m = 0;
  for (i = 1; i <= g->n; i++) {
    n = 0;
    ligne = NULL;
    // lecture nom de la ville
    l = getline(&ligne, &n, fichier);
    ligne[l-1] = 0; // supprimer le \n à la fin
    g->ville[i] = ligne;
    // lecture coordonnées de la ville
    fscanf(fichier, " %d %d ", &g->xcoord[i], &g->ycoord[i]);
  }
    
  fprintf(stderr,"Fin lecture du fichier %s\n", nom_fichier);
  fclose(fichier);
  return g;
}


void affiche_villes(Tgraphe *g) {
  /* Affiche la liste des villes dans le format svg. N'affiche aucune arête. */
  int i;
  for (i=1; i <= g->n; i++) {
    printf("<text x=\"%d\" y=\"%d\" font-size=\"14\"> %s </text>\n",
	   g->xcoord[i], g->ycoord[i], g->ville[i]);
  }
}

void affiche_arete(Tgraphe *g, int i, char *sty) {
  /* Affiche l'arete i du graphe g avec le style sty.
     Le style sty peut etre par exemple :
       "stroke:rgb(200,200,200);stroke-width:4"
       pour une ligne en gris avec une épaisseur de 4.
  */
  int a,b;
  a = g->arete[i].a;
  b = g->arete[i].b;
  
  printf("<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"%s\" />\n",
	 g->xcoord[a], g->ycoord[a], g->xcoord[b], g->ycoord[b], sty);
}

/*************************************************/
/************* Partition des sommets *************/
/*************************************************/
/* Dans l'algorithme de Kruskal, pour determiner si une arête créerait un
   cycle, on vérifie si les extrémités de l'arête sont dans la même
   composante connexe. Pour cela, on utilise une partition des sommets (Cf
   cours et exo 8 du TD).
   Une partition est ici juste un tableau d'entiers p tel que p[s] est 
   le numéro de la partition du sommet s.
*/

/* retourne une partition où chaque sommet est seul dans sa composante */
int *init_partition(int nb) {
  int *p;
  int i;

  p =(int *) malloc((nb+1)*sizeof(int));
  for (i=1; i <= nb; i++) 
    p[i] = i;
  return p;
}

/* fusionne les composantes des sommets a et b */
void fusion(int *p, int nb, int a, int b) {
  int i, comp_b;

  comp_b = p[b];
  for (i=1; i<= nb; i++) 
    if (p[i] == comp_b)
      p[i] = p[a];
}

/* teste si a et b sont dans la même composante */
int meme_composante(int *p, int a, int b) {
  return (p[a] == p[b]);
}

/*************************************************/
/*************************************************/
/*************************************************/

/* fonction à donner en parametre à la fonction qsort pour trier le tableau
   des arêtes (Cf man qsort).*/
int compare_aretes(const void *a, const void *b) {
  double da = ((Tarete *) a)->poids;
  double db = ((Tarete *) b)->poids;
  
  return (int) (da - db);
}

/* Pour calculer le poids de l'arete (u,v) dans le graphe g (c'est la
   distance entre les villes). */
double poids(int u, int v, Tgraphe * g) {
  /* la fonction hypot est definie dans math.h. Cf man hypot. */
  return hypot(g->xcoord[u] - g->xcoord[v], g->ycoord[u] - g->ycoord[v]);
}


double creer_aretes(Tgraphe *g) {
  /* Rajoute les arêtes au graphe g (dans le tableau g->arete). */
  /* Retourne le poids total du graphe g. */
  /*   Le graphe g est complet (il contient toutes les arêtes
       possibles). */
  /*   Le poids d'une arête est calculée avec la foncion poids
       (ci-dessus). */

  /* A COMPLETER */

  /* Il faut commencer par allouer de la mémoire pour 
     le tableau g->arete. Qqchose du type :
     g->arete = malloc(sizeof(Tarete) * XXX);
     ou il faut remplacer XXX par le nombre d'aretes */
  
  /* Le nombre d'aretes doit aussi être stocké dans g->m */
  /* Ensuite, il faut ajouter les arêtes dans le tableau */
    int i, j, k, m, n;
    double p, poids_total;

    /* *** initialisation des variables *** */
    n= g->n;
    m= n * (n - 1) / 2; // nombre d'arrêtes
    g->arete= (Tarete *) malloc(sizeof(Tarete) * m);
    g->m= m;
    k=0;
    poids_total=0;

    /* *** ajout des arêtes dans le graphe *** */
    for(i=1; i<n; i++){
        for(j=i+1; j<=n; j++){
            g->arete[k].a= i;
            g->arete[k].b= j;
            p= poids(i, j, g);
            g->arete[k].poids= p;
            poids_total+= p;
            k++; //arête suivante
        }
    }
  
  return poids_total;
}


double calc_affiche_ACPM(Tgraphe * g) {
  /* Calcule l'ACPM de g avec l'algorithme de Kruskal et affiche au fur et
     à mesure de leur découverte les aretes de l'ACPM au format svg (avec
     la fonction affiche_arete).*/
  /* Retourne le poids de l'acpm trouvé. */
  /* Cette fonction utilisera les fonctions pour manipuler les partitions
     données plus haut (pour savoir quels sommets sont dans la même
     composante connexe). */
  /* Pour trier les arêtes, vous utiliserez la fonction qsort (cf man qsort):
     qsort(g->arete, g->m, sizeof(Tarete), compare_aretes);
   */
    int i, k, n, *composantes;
    double poids_total;

    /* *** initialisation *** */
    poids_total= 0;
    n= g->n;
    qsort(g->arete, g->m, sizeof(Tarete), compare_aretes);
    composantes= init_partition(n);

    affiche_villes(g);

    /* *** construction de l'ACPM *** */
    for(i=1; i<n; i++){
        k=0;
        //on cherche l'arête de poids le plus faible dont la composante de ses 2 sommets sont différentes
        while( meme_composante(composantes, g->arete[k].a, g->arete[k].b) ){
            k++;
        }
        poids_total+= g->arete[k].poids;
        fusion(composantes, n, g->arete[k].a, g->arete[k].b);
        affiche_arete(g, k, "stroke:rgb(200,200,200);stroke-width:4");
    }
    
  
    return poids_total;
}

/* Un paramètre : le nom du fichier contenant les coordonnées des villes */
int main(int argc, char **argv) {
  Tgraphe * g;
  double poids_tot;
  //int i;
  
  if (argc != 2) {
    fprintf(stderr, "Il faut 1 paramètre : nom du fichier contenant les villes.\n");
    exit(1);
  }

  g = lire_graphe(argv[1]);
  poids_tot = creer_aretes(g);
  fprintf(stderr,"Nombre d'arêtes du graphe : %d\n", g->m);
  fprintf(stderr,"Poids total du graphe : %f\n", poids_tot);

  // debut fichier svg
  printf("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"1500\" height=\"850\">\n");
  printf("<rect x=\"0\" y=\"0\" width=\"1500\" height=\"850\" fill=\"white\" />");
  
  /* A COMPLETER */
  /* affiche_villes(g); */
  /* for(i=0; i<g->m; i++){ */
  /*     affiche_arete(g, i, "stroke:rgb(200,200,200);stroke-width:4"); */
  /* } */
  poids_tot= calc_affiche_ACPM(g);
  fprintf(stderr,"Poids total ACPM : %f\n", poids_tot);

  // fin fichier svg
  printf("</svg>\n"); 
  
  return 0;
}
  
      
  
       
