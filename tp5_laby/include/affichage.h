#ifndef AFFICHAGE_H
#define AFFICHAGE_H

/****************************************************************************/
/*******************   fichier affichage.h **********************************/
/****************************************************************************/
/* 
 Vous n'avez pas besoin d'utiliser directement ces fonctions. 

 On peut fixer la couleur du texte et du fond dans un terminal avec des
 séquences de caratères spéciales. Je vous fournis ici des fonctions pour 
 le faire simplement (pour voir les séquences, cf. affichage.c).

 L'affichage (avec printf(...)) se fait à partir de la position
 courante du curseur. On peut deplacer celui-ci avec les fonctions goto_*
*/

int coul_rvb(int r, int v, int b);
// valeurs r,v et b entre 0 et 5 (valeurs de rouge, vert et bleu)
// renvoie le numero de la couleur du triplet r,v,b.
// Ce numéro de couleur peut être utilisé ensuite dans les fonctions couleur_fond et couleur_texte.

int coul_gris(int n);
// niveau de gris n entre 0 (noir)  et 23 (blanc)
// renvoie le numero de la couleur du niveau de gris n.

void couleur_fond(int coul);
/* fixe la couleur de fond : coul entre 0 et 255; 
   on peut utiliser les fonctions coul_rvb et coul_gris pour calculer la
   valeur coul correspondant à un triplet r,v,b ou un niveau de gris voulu */

void couleur_texte(int coul);
/* fixe la couleur du texte : coul entre 0 et 255 */

void reset_couleurs();
/* remet les couleurs de texte et de fond par defaut*/

void efface_ecran();
  /* efface l'ecran du terminal */

void goto_haut_gauche();
  /* place le curseur dans le coin supérieur gauche du terminal */ 

void goto_xy(int x, int y);
  /* place le curseur ligne x colonne y dans le terminal */

void goto_up(int x);
/* deplace le curseur x lignes vers le haut */

void goto_down(int x); 
/* deplace le curseur x lignes vers le bas */

void goto_forward(int x); 
/* deplace le curseur x colonnes vers la droite */

void goto_backward(int x);
/* deplace le curseur x colonnes vers la gauche */

void save_cursor_pos();

void restore_cursor_pos();

#endif
