#include <stdio.h>
#include <stdlib.h>
#include "xmalloc.h"
#include "liste.h"
#include "affichage.h"
#include "parcours.h"
#include "laby.h"

/* ****************************************************************/
/* ********************** fonctions de base  **********************/
/* ****************************************************************/

Tlaby *creer_laby_vide(int largeur, int hauteur) {
  /* Alloue la memoire pour stocker le labyrinthe. 
     Ne fait aucune initialisation. */
  Tlaby *l;

  l = (Tlaby *) xmalloc(sizeof(struct laby));
  l->matrice = (int *) xmalloc((hauteur * largeur + 1) * sizeof(int));
  return l;
}


Tlaby *genere_laby_aleatoire(double p, int l, int h) {
  /* Retourne un labyrinthe aléatoire */
  Tlaby *laby;
  int col, ligne;
  int i;

  if ((h < 3) || (l < 3)) {
    fprintf(stderr, "La largeur et la hauteur doivent etre superieures à 3 l: %d h: %d\n", l, h);
    exit(2);
  }
  /* Allocation de la mémoire pour stocker le graphe */
  laby = creer_laby_vide(l,h);
  /* Remplissage des champs de la structure laby */
  laby->larg = l;
  laby->haut = h;
  /* *********** remplissage de la matrice **************** */
  for (i = 1; i <= l; i++) /* ligne 1 */
    laby->matrice[i] = PLEIN;
  for (i = l+1; i <= (h-1)*l; i++)   /* lignes 2 à h-1 */
    if (i%l == 0 || i%l == 1) /* colonne 1 et l */
      laby->matrice[i] = PLEIN;
    else
      if (p > (double)random()/RAND_MAX) 
	laby->matrice[i] = VIDE;
      else
	laby->matrice[i] = PLEIN;
  for (i = (h-1)*l+1; i <= h*l; i++) /* ligne h */
    laby->matrice[i] = PLEIN;
  
  laby->matrice[l+2] = VIDE; /* La case de départ est mise à VIDE */
  
  /* tirage de la case d'arrivée */
  col = (int) ((double) random()/RAND_MAX*0.999999*(l-2)+2);
  ligne = (int) ((double) random()/RAND_MAX*0.999999*(h-2)+2);
  laby->fin = (ligne-1) * l + col;
  laby->matrice[(ligne-1) * l + col] = VIDE; /* La case d'arrivée est mise à VIDE */
  return laby;
}

Tlaby *lecture_laby(FILE *f){
  /* Lit un labyrinthe dans le fichier f et le renvoie. */
  Tlaby *laby;
  int i, j, numero_sommet, caractere_lu, largeur, hauteur;

  fscanf(f, "%d %d", &hauteur, &largeur);
  getc(f); /* pour consommer le \n à la fin de la 1ere ligne*/

  laby = creer_laby_vide(hauteur,largeur);
  laby->larg = largeur;
  laby->haut = hauteur;
  numero_sommet = 1;
  for (j = 1; j <= laby->haut; j++) {
    for (i = 1; i <= laby->larg; i++) {
      caractere_lu = getc(f);
      switch (caractere_lu) {
	case 'X':
	  laby->fin = numero_sommet;
	case ' ':
	  laby->matrice[numero_sommet] = VIDE;
	  break;
	case '#':
	  laby->matrice[numero_sommet] = PLEIN;
	  break;
	default:
	    fprintf(stderr,
		    "Erreur dans le laby (l: %d, c:%d) : caractere %c invalide\n",
		    j, i, caractere_lu);
	    exit(2);
      }
      numero_sommet++;
    }
    getc(f); /* consomme le \n à la fin de la ligne */
  }
  return laby;
}


/* **************************************************** */
/* **************** Affichage ************************* */
/* **************************************************** */

void affiche_sommet(Tlaby *laby, Tparcours *p, int numero_sommet) {
  /* Le curseur doit être au bon endroit */

  /* choix de la couleur de fond */
  if (laby->matrice[numero_sommet] == PLEIN) {
    couleur_fond(COUL_PLEIN);
  }
  else
    switch (p->couleur[numero_sommet]) {
    case BLANC:
      couleur_fond(COUL_BLANC);
      break;
    case GRIS:
      couleur_fond(COUL_GRIS);
      break;
    case NOIR:
      couleur_fond(COUL_NOIR);
      break;
    case CHEMIN:
      couleur_fond(COUL_CHEMIN);
      break;
    }
  /* choix du caractere à afficher */
  char c = ' ';
  if (numero_sommet == laby->fin) {
    couleur_texte(0);
    c = 'X';} /* sommet final */
  printf("%c", c);
}

void affiche_legende_couleurs() {
  /* affiche la legende des couleurs, comme son nom l'indique */
  printf("Legende :\n");
  printf(" Case pleine : "); couleur_fond(COUL_PLEIN); printf(" "); 
  reset_couleurs();
  printf("     Case vide non explorée : "); couleur_fond(COUL_BLANC); 
  printf(" "); reset_couleurs();
  printf("\n Cases explorées : "); 
  printf(" NOIR : "); couleur_fond(COUL_NOIR); printf(" "); reset_couleurs();
  printf("    GRIS : "); couleur_fond(COUL_GRIS); printf(" "); reset_couleurs();
  printf("    CHEMIN : "); couleur_fond(COUL_CHEMIN); printf(" "); 
  reset_couleurs();
  printf("\n");
}

void affiche_laby(Tlaby *laby, Tparcours *p) {
  /* Affiche le labyrinthe à l'écran, utilise les fonctions de
     affiche.h pour changer la couleur. */
  /* La couleur d'une case est definie par les pseudo constantes 
     définie dans laby.h :
         un sommet PLEIN =>  COUL_PLEIN
	           VIDE non exploré => COUL_BLANC
		   VIDE exploré => COUL_NOIR
		   VIDE en cours d'exploration => COUL_GRIS
		   VIDE appartenant au chemin => COUL_CHEMIN
  */
  int i, j, numero_sommet;

  numero_sommet = 1;
  for (j = 1; j <= laby->haut; j++) {
    couleur_texte(0); // pour que la selection à la souris soit noire
    for (i = 1; i <= laby->larg; i++)
      affiche_sommet(laby, p, numero_sommet++);
    reset_couleurs();
    printf("\n");
  }
}


void reaffiche_sommet(Tlaby *laby, Tparcours *p, int numero_sommet) {
/* Réaffiche le sommet numero_sommet.  
   On suppose que la matrice a deja été affiché et que le curseur se trouve
   sur la 1ere colonne de la ligne suivant la derniere ligne de la matrice.
   Replace le curseur à cet endroit.*/
  int ligne, col;
  int dep_ligne, dep_col;
  
  ligne = (numero_sommet-1) / laby->larg + 1;
  col = (numero_sommet-1) % laby->larg + 1;
  dep_ligne = laby->haut + 1 - ligne;
  dep_col = col -1;
  /* deplace le curseur au bon endroit*/
  goto_up(dep_ligne); 
  goto_forward(dep_col);
  /* affiche le sommet */
  affiche_sommet(laby, p, numero_sommet);
  /* replace le curseur */
  reset_couleurs();
  goto_down(dep_ligne-1); 
  printf("\n"); /* place le curseur au debut de la ligne suivante */
}
