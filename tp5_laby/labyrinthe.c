/* A chaque fois que vous completez une fonction, vérifiez que cela
   fonctionne. Il y a des exemples de labyrinthes dans le répertoire
   ex_labys/ et le répertoire log/ contient des exemples de résultats
   (lisez log/README pour plus d'infos).


Fonctions à completer (voir les détails dans l'énoncé du TP) :
   - gen_graphe() => vérifiez ensuite que le graphe est correct (option -t pour afficher le TLA).
   - visiter() pour le parcours récursif.
   - trace_chemin() : vérifiez que la longueur du chemin renvoyée est correcte.
   - parcours_iteratif() : pour les parcours itératifs en profondeur ou en largeur.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include "include/xmalloc.h"   /* pas besoin de regarder ce fichier */
#include "include/affichage.h" /* pas besoin de regarder ce fichier */

#include "include/liste.h"      /* contient les fonctions sur les listes */
#include "include/graphe_TLA.h" /* Structure Tgraphe et les fonctions pour
				   manipuler un graphe */
#include "include/parcours.h"   /* Structure Tparcours qui contient entre
				 autre les tableaux couleur[], pere[], le
				 nombre de sommets explorés, ...*/
#include "include/laby.h"       /* Structure Tlaby qui contient la matrice
			     décrivant le labyrinthe et le numéro de la case
			     finale. Fonctions pour générer un labyrinthe
			     aléatoire, lire un labyrinthe dans un fichier,
			     afficher le labyrinthe.*/

void milli_sleep(int n)
{
  /* un appel à cette fonction met le processus en sommeil pendant n
     millisecondes, vous pouvez l'utiliser pour ralentir l'affichage dans
     le cas d'une animation du parcours. Il faudra l'appeler juste avant
     de réafficher un sommet. */
  struct timespec t;
  t.tv_sec = 0;
  t.tv_nsec = 1000000 * n;
  nanosleep(&t, &t);
}

Tgraphe *gen_graphe(Tlaby *laby)
{
  /* Retourne le graphe correspondant au labyrinthe passé en paramètre. */
  Tgraphe *g;
  int n, i, l, voisin;

  /* crée un graphe sans arêtes avec le bon nombre de sommets */
  n = laby->haut * laby->larg;
  g = creer_graphe(n);
  /* il faut maintenant rajouter les arêtes */

  /* On parcours la matrices 1 dimension du labi,
     sans s'occuper des bords qui sont tous pleins */
  l = laby->larg;
  for (i = 2; i < n; i++)
  {
    /* si la case est "PLEIN" pas d'arc à rajouter, sinon :*/
    if (laby->matrice[i] != PLEIN)
    {

      /* On regarde la case "au dessus" */
      voisin = i - l;
      if (laby->matrice[voisin] == VIDE)
      {
        ajoute_arc(i, voisin, g);
      }

      /* On regarde la case à droite,  */
      voisin = i + 1;
      if (laby->matrice[voisin] == VIDE)
      {
        ajoute_arc(i, voisin, g);
      }

      /* On regarde la case "en bas", */
      voisin = i + l;
      if (laby->matrice[voisin] == VIDE)
      {
        ajoute_arc(i, voisin, g);
      }

      /* On regarde la case à gauche */
      voisin = i - 1;
      if (laby->matrice[voisin] == VIDE)
      {
        ajoute_arc(i, voisin, g);
      }
    }
  }

  return g;
}

int visiter(Tgraphe *g, Tparcours *p, Tlaby *laby, int s)
{
  /* Parcours en profondeur récursif. */
  /*  Retourne 1 si on trouve le sommet laby->fin et 0 sinon.
      Le parcours s'arrête dès que le sommet fin est trouvé. */
  /* le paramètre laby n'est utile que pour l'affichage s'il y a une
     animation du parcours (cf p->animation dans parcours.h). Dans ce cas,
     il faut utiliser la fonction reaffiche_sommet (dans laby.h) à chaque
     modification de la couleur d'un sommet. */
  int successeur;
  iterateur_l *i_it;
  int trouve = 0, attente = p->attente;

  p->couleur[s] = GRIS;

  /*--animation--*/
  if (p->animation == 1)
  {
    if (attente > 0)
    {
      milli_sleep(attente);
    }
    else if (attente == -1)
    {
      while (getchar() != '\n')
        ;
    }
    reaffiche_sommet(laby, p, s);
  }

  /*--on test si on a fini--*/
  if (s == laby->fin)
  {
    return 1;
  }

  i_it = liste_succ(s, g);
  while (!fini_it(i_it) && trouve == 0)
  {
    successeur = element_it(i_it);

    if (p->couleur[successeur] == BLANC)
    {
      p->pere[successeur] = s;
      trouve = visiter(g, p, laby, successeur);
    }

    i_it = suivant_it(i_it);
  }
  p->couleur[s] = NOIR;

  /*--animation--*/
  if (p->animation == 1)
  {
    if (attente > 0)
    {
      milli_sleep(attente);
    }
    else if (attente == -1)
    {
      while (getchar() != '\n')
        ;
    }
    reaffiche_sommet(laby, p, s);
  }
  p->nb_sommets_explores++;

  return trouve;
}

int trace_chemin(Tparcours *p, int s)
{
  /* Met la valeur CHEMIN dans les cases de p->couleur[] par lesquelles
     passe le chemin de l'arborescence de parcours allant de la racine à s
     et renvoie la longueur de celui-ci.

     On suppose que p->pere contient l'arborescence de parcours et que
     p->pere[racine] vaut -1.*/
  int sommet = p->pere[s], longueur = 0;

  while (sommet != -1)
  {
    p->couleur[sommet] = CHEMIN;
    sommet = p->pere[sommet];
    longueur++;
  }

  return longueur;
}

int parcours_iteratif(Tgraphe *g, Tparcours *p, Tlaby *laby, int s)
{
  /*  En fonction de p->algo, fait un parcours en profondeur ou un parcours
      en largeur. Il faut utiliser les fonctions de include/liste.h pour
      gérer la liste des prochains sommets à explorer (soit en FIFO soit en
      LIFO). */
  /*  Pour récupérer un itérateur sur la liste des successeurs d'un
      sommet, utiliser liste_succ() définie dans include/graphe_TLA.h */
  /*  Retourne 1 si on trouve le sommet laby->fin et 0 sinon.
      Le parcours s'arrête dès que le sommet fin est trouvé. */
  iterateur_l *i_it;
  int sommet, successeur, trouve = 0, attente = p->attente, algo = p->algo;
  Liste *l = creer_liste_vide();

  /*--On rajoute s dans la liste--*/
  empile(s, l);
  p->couleur[s] = GRIS;

  while (!est_vide(l) && trouve == 0)
  {
    if (algo == PROFONDEUR)
    {
      sommet = depile(l);
    }
    else
    {
      sommet = defile(l);
    }

    /*--on récupère le pointeur sur la liste des successeurs pour la parcourir--*/
    i_it = liste_succ(sommet, g);
    while (!fini_it(i_it) && trouve == 0)
    {
      successeur = element_it(i_it);

      /*--on test si on a fini--*/
      if (successeur == laby->fin)
      {
        trouve = 1;
      }

      /*--sinon on test si on doit l'explorer--*/
      if (p->couleur[successeur] == BLANC)
      {
        p->couleur[successeur] = GRIS;

        /*--animation--*/
        if (p->animation == 1)
        {
          if (attente > 0)
          {
            milli_sleep(attente);
          }
          else if (attente == -1)
          {
            while (getchar() != '\n')
              ;
          }
          reaffiche_sommet(laby, p, successeur);
        }

        p->pere[successeur] = sommet;
        if (algo == PROFONDEUR)
        {
          empile(successeur, l);
        }
        else
        {
          enfile(successeur, l);
        }
      }
      i_it = suivant_it(i_it);
    }

    /*--on a fini d'explorer le sommet courant--*/
    p->couleur[sommet] = NOIR;
    p->nb_sommets_explores += 1;

    /*--animation--*/
    if (p->animation == 1)
    {
      if (attente > 0)
      {
        milli_sleep(attente);
      }
      else if (attente == -1)
      {
        while (getchar() != '\n')
          ;
        ;
      }
      reaffiche_sommet(laby, p, sommet);
    }
  }
  return trouve;
}

void usage(int argc, char **argv)
{
  fprintf(stderr, "Usage :\n   %s [Options] largeur hauteur proba [graine]\n", argv[0]);
  fprintf(stderr, "   %s [Options] -f <fichier>\n", argv[0]);

  fprintf(stderr, "Dans le second cas (option \"-f\"), le labyrinthe est lu dans le fichier indiqué.\n\n");

  fprintf(stderr, "Options :\n");
  fprintf(stderr, "  -t   : Affiche le TLA du graphe pour vérification.\n");
  fprintf(stderr, "Choix de l'algo de parcours :\n");
  fprintf(stderr, "  -l : pour un parcours en largeur ;\n");
  fprintf(stderr, "  -p : pour un parcours en profondeur ;\n");
  fprintf(stderr, "  -r : pour un parcours en profondeur récursif ;\n");
  fprintf(stderr, "  sinon, fait juste un affichage du labyrinthe.\n\n");
  fprintf(stderr, "Autres options (uniquement s'il y a un parcours), :\n");
  fprintf(stderr, "  -a   : fait une animation du parcours ;\n");
  fprintf(stderr, "  -w n : attend n ms avant chaque réaffichage du laby lors d'une animation;\n");
  exit(1);
}

int main(int argc, char **argv)
{
  FILE *fichier;
  char *nom_fichier;
  Tgraphe *g;
  Tparcours *p;
  Tlaby *laby;
  int depart, long_chemin, ok;
  int c;
  int algo, animation, attente, arbo, dans_fichier, nb_algo, affiche_tla;
  int larg, haut, graine;
  double proba;

  /* ************ traitement des paramètres de la ligne de commande *****/
  /* comprendre ce qui se passe ici n'est pas indispensable */
  /* pour avoir des infos sur getopt : "man 3 getopt" ou google...*/
  algo = animation = attente = arbo = dans_fichier = nb_algo = affiche_tla = 0;
  while ((c = getopt(argc, argv, "f:lpraw:t")) != -1)
    switch (c)
    {
    case 'f':
      dans_fichier = 1;
      nom_fichier = optarg;
      break;
    case 'l':
      nb_algo++;
      algo = LARGEUR;
      break;
    case 'p':
      nb_algo++;
      algo = PROFONDEUR;
      break;
    case 'r':
      nb_algo++;
      algo = RECURSIF;
      break;
    case 'a':
      animation = 1;
      break;
    case 'w':
      attente = atoi(optarg);
      break;
    case 't':
      affiche_tla = 1;
      break;
    default:
      usage(argc, argv);
    }
  if (nb_algo > 1)
  {
    fprintf(stderr, "\n** Il faut sélectionner un seul algo de parcours\n\n");
    usage(argc, argv);
  }
  if (affiche_tla && animation)
  {
    fprintf(stderr, "\n** Animation (-a) et affichage du tla (-t) incompatibles\n\n");
    usage(argc, argv);
  } /* ************ fin traitement des paramètres   *************/

  /************** Génération du labyrinthe ou lecture dans un fichier (en fonction des paramètres de la ligne de commande)  ********/
  if (dans_fichier)
  {
    /* Lecture du labyrinthe dans un fichier */
    if (optind != argc)
      usage(argc, argv);
    fichier = fopen(nom_fichier, "r");
    if (fichier == NULL)
    {
      fprintf(stderr, "Pb lors de la lecture du fichier %s\n", nom_fichier);
      exit(1);
    }
    laby = lecture_laby(fichier);
    printf("Labyrinthe du fichier \"%s\": largeur = %d, hauteur = %d\n",
           nom_fichier, laby->larg, laby->haut);
  }
  else
  {
    /* Génération aléatoire du labyrinthe */
    if (optind + 4 == argc)
      /* si la graine du générateur aléatoire est passée en paramètre, on la récupère */
      graine = atoi(argv[optind + 3]);
    else if (optind + 3 == argc)
      /* sinon on utilise un xor entre heure actuelle et le PID comme graine */
      graine = (time(NULL) ^ getpid()) & 0xFFF;
    else
      usage(argc, argv);
    srandom(graine); /* initialisation generateur aléatoire avec la graine */
    larg = atoi(argv[optind]);
    haut = atoi(argv[optind + 1]);
    proba = atof(argv[optind + 2]);
    laby = genere_laby_aleatoire(proba, larg, haut);
    printf("Labyrinthe aléatoire : largeur = %d, hauteur = %d, proba = %.3f, graine = %d.\n",
           larg, haut, proba, graine);
  } /************** fin génération ou lecture du labyrinthe ********/

  /* affichage labyrinthe */
  p = creer_parcours_vide(laby->haut * laby->larg); /* crée et initialise p (structure parcours) */
  affiche_legende_couleurs();
  affiche_laby(laby, p);

  /***************** création du graphe à partir du labyrinthe *************/
  g = gen_graphe(laby);
  if (affiche_tla)
    afficheTLA(g); /* affiche le TLA du graphe si l'option -t est utilisée. */
  /************************** fin création graphe *******************/

  /* si aucun algo de parcours sélectionné : c'est fini */
  if (nb_algo == 0)
    return 0;

  /* sinon on prépare le parcours */
  p->algo = algo;           // spécifié par les options -l, -p ou -r de la ligne de commande
  p->animation = animation; // option -a
  p->attente = attente;     // option -w
  depart = laby->larg + 2;  /* sommet de depart : 2e sommet de la 2e ligne */
  p->pere[depart] = -1;

  /* on lance le parcours */
  if (algo == RECURSIF)
    ok = visiter(g, p, laby, depart);
  else
    ok = parcours_iteratif(g, p, laby, depart);

  if (ok)
    /* si il existe un chemin, on le trace */
    long_chemin = trace_chemin(p, laby->fin);

  /* affichage du résultat */
  printf("\nGraphe après parcours :\n");
  affiche_laby(laby, p);
  printf("\n");
  if (ok)
    printf("Longueur du chemin trouvé : %d\n", long_chemin);
  else
    printf("Pas de chemin\n");
  printf("Nombre de sommets explorés : %d\n", p->nb_sommets_explores);
  return 0;
}
