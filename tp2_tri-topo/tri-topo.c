/* Les membres du groupe de TP (groupes de 2 au maximum, redoublants seuls) :
NOM1
NOM2
*/
#include <stdio.h>
#include <stdlib.h>

#include "include/liste.h"

#define NMAX 100  /* nombre maximum de sommets */
#define VRAI 1
#define FAUX 0

Liste *TLA[NMAX+1]; /* tableau des listes d'adjacence */
/* TLA[i] est la liste des successeurs du sommet i. Comme les sommets sont
   numérotés à partir de 1, vous ne devez pas utiliser TLA[0] */
int n; /* le nombre de sommets */


void lire_graphe_tla() {
  /* à écrire, inspirez-vous de la fonction lire_graphe du tp1. Le format d'entrée est le même que dans le tp 1. */
    int i, o, e, m;

    scanf("%d", &n);/*nb de sommets*/
    if (n > NMAX) {
        fprintf(stderr, "PB : Le nombre de sommets %d est plus grand que NMAX = %d\n", n, NMAX);
        exit(1);
    }
    scanf("%d",&m); /* nb d'arcs */

    /*initialisation des listes du TLA*/
    for(i=1; i<=n; i++){
        TLA[i]= creer_liste_vide();
    }
    /*Lecture des arcs (o,e) et replissage du TLA*/
    for(i=1; i<=m; i++){
        scanf("%d %d", &o, &e);
        /*Test si les arguments sont valides*/
        if ((o < 1) || (e < 1) || (o > n) || (e > n)) {
            fprintf(stderr, "arc (%d,%d) non valide\n",o ,e);
            exit(1);
        }
        empile(e, TLA[o]);      
    }
     
}

void affiche_tla() {
  /* Affiche la liste des successeurs de chaque sommet (cf le fichier log
     pour des exemples) */
  /* À completer, utilisez la fonction affiche_liste() de include/liste.h */
    int i;
    
    printf("Tableau des listes d'adjacence :\n");

    /*Affichages des listes de successeurs des sommets*/
    for(i=1; i<=n; i++){
        printf("  successeurs de %d: ", i);
        affiche_liste(TLA[i]);
        printf("\n");
    }
    printf("\n");   
}



int tritopologique(int rang[]) {
  /* Cette fonction effectue un tri topologique. Elle remplit le tableau
     rang[] passée en paramètre.  Elle retourne vrai si le tri topologique
     est possible et faux sinon. */
  /* Vous aurez besoin de declarer une liste des sommets sources l_sources,
     et un tableau dmoins pour les degrés entrants */

    
    Liste *l_sources;
    int dmoins[NMAX];
    int i, s, k=1;/*i indice de dmoins, s pour stocker un sommet, k pour le rang*/
    iterateur_l *a;

    
    /*Initialisation des variables*/
    l_sources= creer_liste_vide();
    for(i=1; i<=n; i++){
        dmoins[i]= 0;
    }

    
    /* Première étape : calcul des degrés entrants en parcourant les
       listes d'adjacences du tableau TLA avec un itérateur*/
    for(i=1; i<=n; i++){
        for(a=init_it(TLA[i]) ; !fini_it(a) ; a=suivant_it(a)){
            dmoins[element_it(a)]++;
        }
    }

    
    /* 2e étape : les sommets de degré entrant nul sont mis dans la liste l_sources 
       et on affiche le degré des sommets, la liste des sources */
    printf("Degrés entrants : ");
    for(i=1; i<=n; i++){
        printf(" d[%d]=%d", i, dmoins[i]);
        if(dmoins[i] == 0){
            empile(i, l_sources);
        }
    }
    printf("\nListe des sources : ");
    affiche_liste(l_sources);
    printf("\n\n");


    /*3e étape : calcul du rang des sommets*/
    while( !est_vide(l_sources) ){
        /* Traitement de la liste l_sources et mise à jour de dmoins */
        s= depile(l_sources);
        rang[s]= k;
        k++;
        printf("sommet choisi : %d\n", s);
        
        for(a= init_it(TLA[s]) ; !fini_it(a) ; a=suivant_it(a)){
            dmoins[element_it(a)]--;
            if(dmoins[element_it(a)] == 0){
                empile(element_it(a), l_sources);
            }
        }
        printf("Degrés entrants : ");
        for(i=1; i<=n; i++){
            printf(" d[%d]=%d", i, dmoins[i]);
        }
        printf("\nListe des sources : ");
        affiche_liste(l_sources);
        printf("\n\n");
    }
    
    /* La fonction retourne VRAI si le tri est fait, et FAUX s'il y a un circuit */
    if(k != n+1){
        return FAUX;
    }
    return VRAI; 
}


int main() {
    int rang[NMAX];
    int tri, traite=1, i=0;

    lire_graphe_tla();
    affiche_tla();
  
    /* à completer une fois que vous avez écrit le tri topologique */
    tri= tritopologique(rang);
    if(tri == VRAI){
        printf("Sommets triés selon un ordre topologique : \n");
        while(traite <= n){
            /*tant que l'on a pas affiché tous les sommets on parcours le tableau rang depuis le 
              début et on cherche celui qui est de rang traite*/
            if(rang[i] == traite){
                printf(" %d", i);
                i=0;
                traite++; 
            }
            i++;
        }
        printf("\n\n");
        printf("Contenu tableau rang :\n");
        printf("       i : ");
        for(i=1; i<=n; i++){
            printf(" %d", i);
        }
        printf("\n rang[i] : ");
         for(i=1; i<=n; i++){
            printf(" %d", rang[i]);
        }       
    }
    else{
        printf("Tri topologique impossible : il y a un circuit dans le graphe\n");
    }
    printf("\n\n");


  
    /* Exemple d'utilisation des fonctions sur les listes (cf include/liste.h) */
    Liste *l;
    l = creer_liste_vide();
    empile(5, l);
    empile(4, l);
    affiche_liste(l);
    printf("\n");
    empile(3, l);
    enfile(6, l);
    affiche_liste(l);
    printf("\n");
    int s = depile(l);
    printf("sommet dépilé: %d\n",s);
    affiche_liste(l);
    printf("\n");
    /* utilisation des iterateurs pour parcourir une liste sans la détruire :*/
    iterateur_l *a;
    a = init_it(l);
    while (! fini_it(a)) {
        s = element_it(a);
        printf("Element: %d\n", s);
        a = suivant_it(a);
    } 
    printf("\nListe : ");
    affiche_liste(l);
    printf("\n");

    return 0;
}

