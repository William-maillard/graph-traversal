# TP-L2-S2-graph-theory

Implementations of algirthm seen during the course 'graph theory':

- topological sorting
- width graph visit
- deep graph visit
- find minimum path length
- minimum weight spanning tree (Prim, Kruskal)
- minimum weight path (Dijkstra)

Here some illustration visit graph algorithms:

## TP5-laby

### width graph visit

![width](doc/screen-shots/width_graph_visit.gif)

### depth graph visit

![depth](doc/screen-shots/depth_graph_visit.gif)

## T7-dijkstra

### minimum weight path

![Dijkstra](doc/screen-shots/dijkstra.gif)

## T8-minimum spanning tree

![minimum spanning tree](doc/screen-shots/graphe_113_villes.svg)
