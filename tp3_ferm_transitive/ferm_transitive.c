/* Les membres du groupe de TP (groupes de 2 au maximum, redoublants seuls) :
NOM1 : MAILLARD
NOM2
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "include/graphe_MADJ.h"

void usage() {
    fprintf(stderr, "Usage :\nCet algorithme de calcul de fermeture reflexo-transitive prend 1 paramètre :\n  ./ferm_transitive 4 : algo en O(n^4)\n  ./ferm_transitive 3 : algo de Roy Warshall en O(n^3)\nLe graphe est lu sur l'entrée standard. On peut aussi utiliser une redirection. Par exemple :\n  ./ferm_transitive 4 < graphes_exemples/g0_3.txt\nOu alors génerer le graphe avec gen_graphe_aleatoire et faire un tube :\n  ./gen_graphe_aleatoire 5 1 | ./ferm_transitive 4\n");
    exit(1);
}

/*___________Fonctions générales__________*/

/*--fait l'union de deux graphes, dans le 1°graphes, de même nombre de sommets donné en paramètre --*/
void union_graphes(TGraphe *Gunion, TGraphe *G2, int n){
    int j, k;
    
    for(j=1; j<=n; j++){
        for(k=1; k<=n; k++){
            if(arc(j, k, G2)){
                ajoute_arc(j, k, Gunion);
                    
            }
        }
    }
}

/*--ferme reflexivement le graphe passé en paramètre--*/
void fermeture_reflexive(TGraphe *Gplus, int n){
    int i;
    for(i=1; i<=n; i++){
        ajoute_arc(i, i, Gplus);
    }
}
/*_______________________________________*/

/*___________Calcul de la fermeture transitive n4_____________*/
/*
  calcul et renvoie G^(k+1) à partir de G^k,
  complexité en "petit thau" de n^3
*/
TGraphe *calcul_G_k_plus_un(TGraphe *G, TGraphe *G_k, int n){
    TGraphe *G_k_plus_un= creer_graphe(nb_sommets(G));
    int x, y, z;

    for(x=1; x<=n; x++){
        for(z=1; z<=n; z++){
            for(y=1; y<=n; y++){
                if(arc(x, z, G_k)  &&  arc(z, y, G)){
                    ajoute_arc(x, y, G_k_plus_un);
                }
            }
        }
    }

    return G_k_plus_un;
} 


TGraphe *ferm_transitive_n4 (TGraphe *G){
    /* Algorithme en O(n^4)
       Votre algorithme doit calculer et afficher les puissances de G : G^2, G^3 ...
       Vous pouvez creer des fonctions auxiliaires.
    */
    TGraphe *G_k, *Gplus;
    int n, i;

    /*--initialisation des variables--*/
    n= nb_sommets(G);
    G_k= creer_graphe(n);
    Gplus= creer_graphe(n);
    G_k= G;

    /*--calcul des G exposant K, k<=n--*/
    union_graphes(Gplus, G_k, n);
    for(i=2 ; i<=n ; i++){
        G_k= calcul_G_k_plus_un(G, G_k, n);
        printf("Matrice d'adjacence de G^%d :\n", i);
        afficheMADJ(G_k);
        printf("\n");

        /*on rajoute G^k à l'union*/
        union_graphes(Gplus, G_k, n);
    }

    fermeture_reflexive(Gplus, n);
    
    /*On détruit G_k qui ne va plus servir*/
    detruit_graphe(G_k);

    return Gplus; // à modifier: doit retourner la fermeture reflexo-transitive.
}
/*____________________________________________________________*/

/*___________Calcul de la fermeture transitive n3_____________*/

/*
calcul et renvoie le graphe G_inf_K+1 à partir de G et G_inf_k,
complexité en "thau" de n^2.
*/
TGraphe *calcul_Ginf_kplusun(TGraphe *Ginfk, int k, int n){
    TGraphe *Ginf_kplusun= NULL;
    int i, j, kplusun;

    /*--initialisation des variables--*/
    Ginf_kplusun= creer_graphe(n);
    Ginf_kplusun= Ginfk;
    kplusun= k+1;

    /*--Calcul de Ginf_kplusun--*/
    for(i=1; i<=n; i++){
        for(j=1; j<=n; j++){
            if( arc(i, kplusun, Ginfk)  &&  arc(kplusun, j, Ginfk) ){
                ajoute_arc(i, j, Ginf_kplusun);
            }
        }
    }

    /*--Renvoie de Ginf_kplusun--*/
    return Ginf_kplusun;
}


TGraphe *ferm_transitive_n3 (TGraphe *G){
    // Algorithme de Roy Warshall en O(n^3)
    TGraphe *Gk=NULL, *Gplus=NULL;
    int n, k;

    /*--initialisation des variables--*/
    n= nb_sommets(G);
    Gk= creer_graphe(n);
    Gk= G;
    Gplus= creer_graphe(n);
    
    printf("Ajout des boucles.\n\n");
    fermeture_reflexive(Gk, n);

    /*--Calcul de Gplus--*/
    for(k=0; k<n; k++){
        Gk= calcul_Ginf_kplusun(Gk, k, n);
        printf("Matrice après ajout des chemins passant par %d :\n", k+1);
        afficheMADJ(Gk);
        printf("\n");

        union_graphes(Gplus, Gk, n);
    } 
 
    detruit_graphe(Gk);  
    return Gplus;
}

/*____________________________________________________________*/

void tst_fct_graphe() {
    TGraphe *G;
  
    /* Exemple d'utilisation des fonctions sur les graphes (ces fonctions sont dans include/graphe_MADJ.h). */
    G = creer_graphe(5);
    ajoute_arc(3,4,G);
    ajoute_arc(1,5,G);
  ajoute_arc(1,5,G);  // ajouter un arc deja dans le graphe ne change rien
  ajoute_arc(2,2,G);
  enleve_arc(1,3,G);  // enlever un arc qui n'est pas dans le graphe ne change rien
  printf("Le graphe G a %d sommets\n", nb_sommets(G));
  afficheMADJ(G);
  
  if (arc(3,4,G))
     printf("Le graphe G contient l'arc (3,4)\n");
  else
    printf("Le graphe G ne contient pas l'arc (3,4)\n");

  if (arc(5,1,G))
     printf("Le graphe G contient l'arc (5,1)\n");
  else
    printf("Le graphe G ne contient pas l'arc (5,1)\n");
  detruit_graphe(G);
}


int main(int argc, char* argv[]){
    int algo; /* 3 ou 4 : indique l'algo à utiliser */
    TGraphe *G;
    clock_t debut, fin; /* variables pour stocker le temps */
    double duree;
    
    /* Pour tester les fonctions sur les graphes (cette fonction est définie juste au
       dessus). A commenter ensuite. 
       tst_fct_graphe();
    */
  
  if (argc != 2) usage(); // il faut un parametre pour indiquer l'algo.

  /* l'algo à utiliser pour la fermeture reflexo-transitive */
  algo = atoi(argv[1]);
  /* si algo == 3 => algo de Roy Warshall,
     si algo == 4 => algo en O(n^4)
     sinon erreur de parametre */

  
  /* à completer */
  if(algo != 3  &&  algo != 4){
      usage();
      exit(-1);
  }

  G= lire_graphe();
  printf("Matrice d'adjacence du graphe G :\n");
  afficheMADJ(G);
  printf("\n");

  
  if(algo == 4){
      printf("**** Calcul avec l'algorithme en O(n^4) :\n\n");
      debut= clock();
      G= ferm_transitive_n4(G);
      fin= clock();
  }
  if(algo == 3){
      printf("**** Calcul avec l'algorithme de Roy Warshall en O(n^3) :\n\n");
      debut= clock();
      G= ferm_transitive_n3(G);
      fin= clock();
  }

  /*--affichage de la durée du calcul dans le canal d'erreurs --*/
  duree= ((double)(fin - debut)) / CLOCKS_PER_SEC;
  fprintf(stderr,"%d %f\n", nb_sommets(G), duree);
  
  printf("Fermeture reflexo-transitive de G :\n");
  afficheMADJ(G);

  detruit_graphe(G);
  exit(0);
}


/*__________Réponse aux questions sur le temps d'exécution__________*/

/*--nb de sommets max pour un temps d'exécution < à 5s--*/
/*
commande utilisé :
>gen_graphe_aleatoire N 3 | ferm_transitive 3 2>> temps_n3 >/dev/null
où N est le nombre de sommet que l'on fait varier par tatonement.

Pour l'algo en n^3:
n=465 t=4.985288
n=466 t=5.117988

(meme commande mais on change le paramètre du prog et le nom du fichier: temps_n4)
Pour l'algo en n^4:
n=139 t=4.969148
n=140 t=5.248095
*/

/*--évolution du temps d'exécution lorsque l'on double les sommets--*/
/*
(on utilise toujours la même commande)

avec la commande :
>cat /proc/cpuinfo
On trouve que le processeur à une fréquence d'environ 800Mhz= 800.10⁶ opérations élementaires/secondes. 
algo en petit thau de n^3 -> on fait n^3 opération.
On divise ce nombre par la fréquence du processeur pour avoir le temps théorique d'exécution.

Pour l'algo en n^3:
( (n*2)^3 = n^3 * 2^3 = n^3 * 8 )  
______________________________________________________________
|n=  |  t (en s)  | évolution de t ~=| temps théorique (en s)|
|50  |  0.030785  | _                | 0.00015625            |
|100 |  0.072979  | x2.370           | 0.00125               |
|200 |  0.407843  | x5.589           | 0.01                  |
|400 |  3.122106  | x7.655           | 0.08                  |
|____________________________________|_______________________|

Pour l'algo en n^4:
( (n*2)^4 = n^4 * 2^4 = n^4 * 16 )
______________________________________________________________
|n=  |  t=        | évolution de t ~=| temps théorique (en s)|
|50  |  0.099562  | -                | 0.0078125             |
|100 |  1.334661  | x13.405          | 0.125                 |
|200 |  21.099160 | x15.808          | 2                     |
|____________________________________|_______________________|

Conclusion:
L'évolution du temps d'exécution est cohérente avec la complexité théorique pour les grandes valeurs de n.
 */