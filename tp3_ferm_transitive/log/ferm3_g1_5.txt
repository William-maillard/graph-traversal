Matrice d'adjacence du graphe G :
               
     1 2 3 4 5 
  1: . X . . . 
  2: . X . . X 
  3: . . . . X 
  4: . . . . . 
  5: . . X X . 

**** Calcul avec l'algorithme de Roy Warshall en O(n^3) :

Ajout des boucles.

Matrice après ajout des chemins passant par 1 :
               
     1 2 3 4 5 
  1: X X . . . 
  2: . X . . X 
  3: . . X . X 
  4: . . . X . 
  5: . . X X X 

Matrice après ajout des chemins passant par 2 :
               
     1 2 3 4 5 
  1: X X . . X 
  2: . X . . X 
  3: . . X . X 
  4: . . . X . 
  5: . . X X X 

Matrice après ajout des chemins passant par 3 :
               
     1 2 3 4 5 
  1: X X . . X 
  2: . X . . X 
  3: . . X . X 
  4: . . . X . 
  5: . . X X X 

Matrice après ajout des chemins passant par 4 :
               
     1 2 3 4 5 
  1: X X . . X 
  2: . X . . X 
  3: . . X . X 
  4: . . . X . 
  5: . . X X X 

Matrice après ajout des chemins passant par 5 :
               
     1 2 3 4 5 
  1: X X X X X 
  2: . X X X X 
  3: . . X X X 
  4: . . . X . 
  5: . . X X X 

Fermeture reflexo-transitive de G :
               
     1 2 3 4 5 
  1: X X X X X 
  2: . X X X X 
  3: . . X X X 
  4: . . . X . 
  5: . . X X X 
