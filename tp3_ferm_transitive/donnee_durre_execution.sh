#!/usr/bin/env bash

function usage(){
    echo "donnez deux paramètres : la complexite de l'algo (3 ou 4) et le chemin du fichier ou seront stockés les données.\n"
}

if (( $# != 2 ))
then
    usage
    exit 0
else 
    n=10
    if (( $1 == 3 ))
    then
        while (( n <= 470 ))
        do
            (gen_graphe_aleatoire $n 3 | ferm_transitive $1 2>> $2 >/dev/null)
            n=$(( $n + 10 ))
            echo $n
        done
    else
        while (( n <= 140 ))
        do
            gen_graphe_aleatoire $n 3 | ferm_transitive $1 2>> $2 >/dev/null
            n=$(( $n + 2 ))
        done
    fi
fi

