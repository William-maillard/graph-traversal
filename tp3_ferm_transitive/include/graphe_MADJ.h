#ifndef GRAPHE_MADJ
#define GRAPHE_MADJ

/* Le type utilisé pour les graphes est : TGraphe */
typedef struct tgraphe TGraphe;


/****************************************************************/
/*         Opérations de base sur les graphes                   */
/****************************************************************/

/* Créé et renvoie un pointeur sur un 1-graphe orienté à n sommets et sans arc. */
/* Les sommets sont numérotés de 1 à n.  */
TGraphe *creer_graphe(int n);

/* Renvoie le nombre de sommets du graphe G */
int nb_sommets(TGraphe *G);

/* Rajoute l'arc (origine, extremité) au graphe G. Si l'arc existe déjà: ne fait rien */
void ajoute_arc(int origine, int extremite, TGraphe *G);

/* Retire l'arc (origine, extremité) au graphe G. Si l'arc n'existe pas: ne fait rien */
void enleve_arc(int origine, int extremite, TGraphe *G);

/* Renvoie vrai si l'arc existe dans G */
int arc(int origine, int extremite, TGraphe *G);

/* Détruit le graphe G et libère la mémoire utilisée */
void detruit_graphe(TGraphe *G);

/*******************************************************************/
/*         Autres operations sur les graphes (affichage, lecture)  */
/*******************************************************************/

/* Affiche la matrice d'adjacence avec des . et des X  */
/*   . si l'arc n'existe pas (si la valeur dans la matrice d'adjacence est nulle) */
/*   X si l'arc existe (si la valeur dans la matrice d'adjacence est non nulle) */
void afficheMADJ(TGraphe *G);


/* Lit un graphe sur l'entrée standard. */
/* Cette fonction crée la structure TGraphe et renvoie un pointeur dessus. */
/* Le format utilisé pour lire le graphe est celui des tp 1 et 2 : */
/*   1ere ligne : nb de sommets */
/*   2e ligne : nb d'arcs */
/*   lignes suivantes arcs (a,b) sous la forme : a b  */
TGraphe *lire_graphe(void);


/* Affiche le graphe dans le format utilisé par lire_graphe */
/* Utilisé par gen_graphe_aleatoire */
void ecrire_graphe(TGraphe *G);

/* Affiche la matrice d'adjacence (les valeurs stockées dans la matrice) */
void affiche_matrice(TGraphe *G);


#endif
