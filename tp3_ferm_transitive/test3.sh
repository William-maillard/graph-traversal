#!/bin/bash

echo $(basename $0)
if [[ $(basename $0) == "test3.sh" ]]; then
    n=3
elif [[ $(basename $0) == "test4.sh" ]]; then
    n=4
else
    exit 4 
fi

mkdir -p meslog/

for f in graphes_exemples/g*.txt; do
    dest=meslog/ferm${n}_$(basename $f)
    echo "Debut calcul fermeture de $f"
    date
    echo "./ferm_transitive $n < $f > $dest"
    ./ferm_transitive $n < "$f" > "$dest"
done

# on peut faire un
# diff -qs meslog/ log/

