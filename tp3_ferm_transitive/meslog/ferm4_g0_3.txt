Matrice d'adjacence du graphe G :
           
     1 2 3 
  1: . . . 
  2: . . X 
  3: X X . 

**** Calcul avec l'algorithme en O(n^4) :

Matrice d'adjacence de G^2 :
           
     1 2 3 
  1: . . . 
  2: X X . 
  3: . . X 

Matrice d'adjacence de G^3 :
           
     1 2 3 
  1: . . . 
  2: . . X 
  3: X X . 

Fermeture reflexo-transitive de G :
           
     1 2 3 
  1: X . . 
  2: X X X 
  3: X X X 
