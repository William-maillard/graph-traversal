#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>

#include "include/graphe_MADJ.h"

int main(int argc, char* argv[]){

  int i,j,n;
  double a,r,t;

  if (argc != 3 && argc != 4){
    printf("usage: gen_graphe_aleatoire nb_sommets nb_moyen_successeurs [graine]\n");
    exit(-1);
  }

  n = atoi(argv[1]);
  a = atof(argv[2]);
  t = (double)a/n;

  TGraphe *G = creer_graphe(n);

  if (argc == 4)
    srandom(atoi(argv[3]));
  else
    srandom(time(NULL) ^ getpid());

  for(i=1; i<=n; i++){
    for(j=1; j<=n; j++){
      r = (double)random()/RAND_MAX;
      if(t>=r)
	ajoute_arc(i,j,G);
    }
  }

  ecrire_graphe(G);
  exit(0);

}
