/* Les membres du groupe de TP (groupes de 2 au maximum, redoublants seuls) :
NOM1: MAILLARD
*/
#include <stdio.h>
#include <stdlib.h>

#define NMAX 100  /* nombre maximum de sommets */

int G[NMAX+1][NMAX+1];  /* la matrice d'adjacence du graphe */
int n; /* le nombre de sommets */

void lire_graphe() {
  int i,j,o,e,m;

  scanf("%d",&n); /* nb de sommets */
  if (n > NMAX) {
    fprintf(stderr, "PB : Le nombre de sommets %d est plus grand que NMAX = %d\n", n, NMAX);
    exit(1);
  }
  scanf("%d",&m); /* nb d'arcs */
  
  for (i = 1; i <= n; i++) 
    for (j = 1; j <= n; j++) 
      G[i][j] = 0;
  
  for (i = 1; i <= m; i++) {
    /* lecture de l'arc (o,e) */
    scanf("%d", &o);
    scanf("%d", &e);
    if ((o < 1) || (e < 1) || (o > n) || (e > n)) {
      fprintf(stderr, "arc (%d,%d) non valide\n",o ,e);
      exit(1);
    }
    G[o][e] = 1;
  }
}



void afficheMADJ() {
  /* à completer */
  int i, j;
  int dplus[n+1], dmoins[n+1];

  /*initialisation des tableaux*/
  for(i=0; i<=n; i++){
    dplus[i]= 0;
    dmoins[i]= 0;
  }

  /*affichage de la matrice d'adjacence*/
  printf("  Matrice d'adjacence :\n");
  printf("   1 2 3 4 5 6 7 8\n");

  for(i=1; i<=n; i++){
    printf("%d: ", i);
    for(j=1; j<=n; j++){
      
      if(G[i][j] == 1){
	printf("X ");
	/*On augmente les degre entrant de j et sortant de i*/
	dmoins[i]++;
	dplus[j]++;
      }
      
      else{
	printf(". ");
      }
    }
    printf("\n");
  }
  
  /*affichage des degrés des sommets*/
  printf("  ");
  for(i=1; i<=n; i++){
    printf("%d ", dplus[i]);
  }
  printf(": degres entrants\n  ");
  
  for(i=1; i<=n; i++){
    printf("%d ", dmoins[i]);
  }
  printf(": degres sortants\n\n");

  
  /*On affiche les sommets isoles*/
  printf("Sommets isoles :");
  for(i=1; i<=n; i++){
    if( (dplus[i]+dmoins[i]) == 0){
      printf(" %d", i);
    }
  }
  printf("\n");  
  

  /*On affiche le nombre de sommets puits*/
  printf("Sommets puits :");
  for(i=1; i<=n; i++){
    if(dmoins[i] == 0){
      printf(" %d", i);
    }
  }
  printf("\n");
 
  /*On affiche le nombre de sommets sources*/
  printf("Sommets sources :");
  for(i=1; i<=n; i++){
    if( dplus[i] == 0){
      printf(" %d", i);
    }
  }
  printf("\n\n");
}




/*retourne 0 si le graphe ne contient pas de trou noir, sinon retourne le numéro du sommet qui est le trou noir*/
int trou_noir(){
  int j=1;
  int sommet_courant= 1, successeur= 0, fini= 0;

  /*étape 2*/
  printf("Suite des sommets courants : 1");
    
  while(fini == 0){
    
    do{
      /*On prends un successeur et on test si il convient*/
      if( G[sommet_courant][j] == 1){
	successeur= j;
      }
    }while(sommet_courant > successeur  &&  ++j <= n);

    if(sommet_courant < successeur){
      /*on a trouve un successeur*/
      sommet_courant= successeur;
      printf(" %d", sommet_courant);
      successeur= 0;
    }
    else{
      /*le sommet a pas de successeur*/
      fini= 1;
      printf("\n");
    }
  }

  /*étape 3*/
  /*
    Le sommet courant est le seul qui peut etre un trou noir car il n'a pas d'arc sortant 
    Or un trou noir a un arc entrant de tous les sommets ce qui n'est donc plus possibles pour les autres sommets.
  */
  /*
    Pour trouver si c'est un trou noir on va vérifier qu'il a un arc entrant de tous les sommets 
    = colonne n°(sommet_courant) contient que des 1 sauf a la ligne n°sommet_courant.
  */
  for(j=1; j<=n && fini == 1; j++){
    if(G[j][sommet_courant] == 0){
      if(j != sommet_courant){
      fini= 3;
      }
    }
  }

  if(fini == 1){ /*on a trouve que des 1 dans la colonne*/
    return sommet_courant;
  }

  return 0;  
  
}


int main() {
  /* à completer */
  int sommet_trou_noir;

  lire_graphe();
  afficheMADJ();
  sommet_trou_noir= trou_noir();
  
  if(sommet_trou_noir == 0){
    printf("Il n'y a pas de trou noir dans le graphe\n");
  }
  else{
    printf("Le sommet %d est un trou noir\n", sommet_trou_noir);
  }

  return 0;
}
